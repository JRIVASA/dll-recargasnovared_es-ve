VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form FrmPhoneServices 
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11490
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11490
   ScaleWidth      =   15330
   Begin VB.Frame FrameDivCategories 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   570
      Left            =   0
      TabIndex        =   8
      Top             =   795
      Width           =   15675
      Begin VB.Label lblTitle1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Visualice y seleccione una de estas categor�as."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   480
         Left            =   0
         TabIndex        =   9
         Top             =   20
         Width           =   15360
      End
   End
   Begin VB.CommandButton CmdNext 
      Enabled         =   0   'False
      Height          =   1455
      Left            =   14070
      Picture         =   "FrmPhoneServices.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   9480
      Visible         =   0   'False
      Width           =   1270
   End
   Begin VB.CommandButton CmdBack 
      CausesValidation=   0   'False
      Enabled         =   0   'False
      Height          =   1455
      Left            =   0
      Picture         =   "FrmPhoneServices.frx":67E2
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   9480
      Visible         =   0   'False
      Width           =   1270
   End
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   8760
      ScaleHeight     =   495
      ScaleWidth      =   2415
      TabIndex        =   4
      Top             =   30
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Frame FrameDivTopSold 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   690
      Left            =   0
      TabIndex        =   3
      Top             =   8280
      Width           =   15675
      Begin VB.Label lblTitle2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Escoja uno de nuestro Top de Productos mas vendidos."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   26.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   600
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   15360
      End
   End
   Begin VB.Frame FrameStellarBar 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   570
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   15675
      Begin VB.Label LbWebsite 
         BackColor       =   &H00404040&
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   120
         Width           =   2295
      End
      Begin VB.Image CmdClose 
         Height          =   480
         Left            =   14640
         Picture         =   "FrmPhoneServices.frx":CFC4
         Top             =   45
         Width           =   480
      End
   End
   Begin VB.Timer TimerKeepAnimation 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   7350
      Top             =   1200
   End
   Begin SHDocVwCtl.WebBrowser WebDoc 
      Height          =   480
      Left            =   7275
      TabIndex        =   0
      Top             =   1800
      Width           =   600
      ExtentX         =   1058
      ExtentY         =   847
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.Image CmdCierre 
      Appearance      =   0  'Flat
      Height          =   720
      Left            =   14160
      Picture         =   "FrmPhoneServices.frx":ED46
      Stretch         =   -1  'True
      Top             =   7200
      Width           =   720
   End
   Begin VB.Image ImgProductDesignArea 
      Height          =   1755
      Index           =   3
      Left            =   10920
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgProductDesignArea 
      Height          =   1755
      Index           =   2
      Left            =   7820
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgProductDesignArea 
      Height          =   1755
      Index           =   1
      Left            =   4700
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgProduct 
      Height          =   1755
      Index           =   0
      Left            =   1550
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgGeneralTopup 
      Height          =   6495
      Left            =   120
      Picture         =   "FrmPhoneServices.frx":15528
      Stretch         =   -1  'True
      Top             =   1560
      Width           =   15015
   End
End
Attribute VB_Name = "FrmPhoneServices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public AllProductsData As Object
Private TopSoldProductsData As Object
Private FilteredData As Object

Private ButtonMatrix As Collection

Private CurrentButtonPanelIndex As Integer

Private InitialButtonPanelMargin As Long

Private ButtonTopMargin As Long
Private ButtonLeftMargin As Long
Private ButtonHeight As Long
Private ButtonWidth As Long

Private TmpRowHeight As Long
Private TmpColWidth As Long

Private i As Integer, FormLoaded As Boolean

Private Sub CmdBack_Click()
    PreviousButtonPanel
End Sub

Private Sub CmdCierre_Click()
    
    If StellarProductID = 853 Then ' Stellar POS
        
        If InputParams("NivelCierreServiciosPrepago") > 0 And InputParams("Nivel_Cajero") < InputParams("NivelCierreServiciosPrepago") Then
            
            TmpAuthData = InputParams("FrmAutorizaciones").DatosUltimaAutorizacion
            
            Load InputParams("FrmAutorizaciones")
            
            InputParams("FrmAutorizaciones").Show vbModal
            
            If TmpAuthData(0) Then
                
                If TmpAuthData(3) < InputParams("NivelCierreServiciosPrepago") Then
                    TmpAuthData(0) = False
                End If
                
                If TmpAuthData(0) Then
                    InputParams("f_Punto").NOVARED_GrabarAutorizacion_CallBack _
                    InputParams("CodigoOperacionAutorizadaCierreServiciosPrepago")
                End If
                
            End If
            
        Else
            TmpAuthData = Array(True)
        End If
        
        If Not TmpAuthData(0) Then
            Mensaje True, "No ha sido autorizado para realizar el Cierre de Operaciones de Servicios."
            Exit Sub
        Else
            
            If InputParams("Conectado_Linea") Then
                
                DoEvents
                
                On Error GoTo ErrorSolicitud
                
                Set DatosCierre = InputParams("f_Punto").NOVARED_RecuperarDatosCierreOnline_CallBack
                
                If DatosCierre("TotalNumberOfServices") <= 0 Then
                    Mensaje True, "No se han realizado transacciones para hacer el cierre. puede ignorar este mensaje y continuar."
                    Exit Sub
                End If
                
                Dim OrderDateParam As String, OrderDate As Date
                Dim Success_Step1 As Boolean, Success_Step2 As Boolean
                
                OrderDate = Now
                OrderDateParam = Format(OrderDate, "YYYYMMDD")
                
                TmpRequest = "<x:Envelope xmlns:x=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ws=""http://ws.novared.com.ve"">" & _
                    "<x:Header/>" & _
                    "<x:Body>" & _
                        "<ws:executeCloseout>" & _
                            "<ws:userCompany>" & Company_ID & "</ws:userCompany>" & _
                            "<ws:user>" & Company_UserID & "</ws:user>" & _
                            "<ws:posId>" & Company_POS_ID & "</ws:posId>" & _
                            "<ws:securityToken>" & TmpSecurityToken & "</ws:securityToken>" & _
                            "<ws:totalTransaction>" & DatosCierre("TotalNumberOfServices") & "</ws:totalTransaction>" & _
                            "<ws:totalAmount>" & DatosCierre("TotalServiceAmount") & "</ws:totalAmount>" & _
                            "<ws:closeoutDate>" & OrderDateParam & "</ws:closeoutDate>" & _
                            "<ws:serialsToConfirm>" & DatosCierre("UnconfirmedSerials") & "</ws:serialsToConfirm>" & _
                        "</ws:executeCloseout>" & _
                    "</x:Body>" & _
                "</x:Envelope> "
                
                'TmpSOAPUrl = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService"
                'TmpSOAPAction = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService/executeCloseout"
                
                TmpSOAPUrl = ServiceURL
                TmpSOAPAction = ServiceURL & "/executeCloseout"
                
                If DebugMode Then
                    Mensaje True, TmpRequest
                    Mensaje True, TmpSOAPAction
                End If
                
                Dim RequestDocument         As MSXML2.DOMDocument60
                Dim ResponseDocument        As MSXML2.DOMDocument60
                Dim CloseOutQuery           As MSXML2.ServerXMLHTTP
                Dim TmpResponse             As String
                
                If DebugMode Then
                    Mensaje True, "Instanciando Objetos DOMDocument60, MSXML2.ServerXMLHTTP"
                End If
                
                Set RequestDocument = New DOMDocument60
                Set CloseOutQuery = New MSXML2.ServerXMLHTTP
                
                If DebugMode Then
                    Mensaje True, "Procediendo a cargar Request en el RequestDocument"
                End If
                
                RequestDocument.async = False
                RequestDocument.loadXML TmpRequest
                
                CloseOutQuery.open "POST", TmpSOAPUrl, False
                
                CloseOutQuery.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
                CloseOutQuery.setRequestHeader "SOAPAction", TmpSOAPAction
                
                DoEvents
                
                If DebugMode Then
                    Mensaje True, "Enviando RequestDocument: " & vbNewLine & vbNewLine & _
                    RequestDocument.xml
                End If
                
                CloseOutQuery.send RequestDocument.xml
                
                DoEvents
                
                TmpResponse = CloseOutQuery.responseText
                
                Set ResponseDocument = New DOMDocument60
                
                If DebugMode Then
                    Mensaje True, "Raw Response: " & vbNewLine & TmpResponse & vbNewLine & vbNewLine & _
                    "Intentando cargar ResponseDocument"
                End If
                
                If (ResponseDocument.loadXML(TmpResponse) And Trim(TmpResponse) <> vbNullString) Then ' Respuesta de Solicitud.
                    
                    If DebugMode Then
                        Mensaje True, "ResponseDocument cargado. Procediendo a habilitar Namespaces y obtener ns1:executeCloseoutReturn"
                    End If
                    
                    ResponseDocument.setProperty "SelectionLanguage", "XPath"
                    
                    ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
                    
                    Set executeCloseoutResponse = ResponseDocument.selectSingleNode("/soapenv:Envelope/soapenv:Body/ns1:executeCloseoutResponse")
                    
                    ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
                    
                    Set executeCloseoutReturn = executeCloseoutResponse.selectSingleNode("ns1:executeCloseoutReturn")
                    
                    Set Result = New DOMDocument60
                    
                    Result.setProperty "SelectionLanguage", "XPath"
                    
                    If DebugMode Then
                        Mensaje True, "executeCloseoutReturn.Text:" & vbNewLine & executeCloseoutReturn.Text
                    End If
                    
                    If Result.loadXML(executeCloseoutReturn.Text) Then
                        
                        If DebugMode Then
                            Mensaje True, "Result Cargado. Evaluando respuesta: " & _
                            Result.xml
                        End If
                        
                        TmpResponseCode = Result.selectSingleNode("result/response_code").Text
                        TmpMessage = Result.selectSingleNode("result/user_message").Text
                        
                        If TmpResponseCode = "00" Then
                            
                            TmpLogCode = Result.selectSingleNode("result/log_code").Text
                            TmpTotalTransactions = Result.selectSingleNode("result/total_txn").Text
                            TmpTotalAmount = Result.selectSingleNode("result/total_amount").Text
                            
                            Set TmpReceiptObj = Result.selectSingleNode("result/receipt")
                            
                            If Not TmpReceiptObj Is Nothing Then
                                
                                TmpReceipt = vbNullString
                                
                                TmpReceipt = TmpReceipt & "** Cierre de Operaciones NOVARED **"
                                
                                TmpReceipt = TmpReceipt & GetLines(2) & "C�digo de Validaci�n: " & TmpLogCode
                                
                                TmpReceipt = TmpReceipt & GetLines(2) & "N�mero de Transacciones: " & _
                                Format(TmpTotalTransactions, "Standard")
                                
                                TmpReceipt = TmpReceipt & GetLines(2) & "Total General: " & _
                                Format(TmpTotalAmount, "Standard")
                                
                                TmpReceipt = TmpReceipt & GetLines(2) & "**** Resumen por Compa�ia de Servicio ****"
                                
                                For Each TmpReceiptRow In TmpReceiptObj.childNodes
                                    
                                    TmpReceipt = TmpReceipt & GetLines(2) & "** " & _
                                    TmpReceiptRow.selectSingleNode("company").Text & " **"
                                    
                                    TmpReceipt = TmpReceipt & GetLines(2) & "Transacciones: " & _
                                    Format(TmpReceiptRow.selectSingleNode("txn").Text, "Standard")
                                    
                                    TmpReceipt = TmpReceipt & GetLines(2) & "Monto Total: " & _
                                    Format(TmpReceiptRow.selectSingleNode("amount").Text, "Standard")
                                    
                                Next
                                
                                TmpReceipt = TmpReceipt & GetLines(2) & "** FIN **"
                                
                            Else
                                TmpReceipt = "Ticket de Cierre No Disponible"
                            End If
                            
                            Success_Step1 = True
                            
                        Else
                            Mensaje True, "No se pudo realizar el cierre. Informaci�n adicional: " & GetLines & TmpMessage
                        End If
                        
                    Else
                        Err.Raise 1
                    End If
                    
                Else
                    
                    Err.Raise 1
                    
ErrorSolicitud:
                    
                    If Err.Number <> 0 Then
                        TmpMessage = "No se recibi� respuesta por parte del proveedor de servicios." & _
                        "El cierre no fue procesado. " & _
                        IIf(Err.Description <> vbNullString, "Informaci�n t�cnica adicional: " & Err.Description, vbNullString)
                    End If
                    
                    Mensaje True, TmpMessage
                    
                End If
                
                If Success_Step1 Then
                    
                    On Error GoTo ErrorConfirmacion
                    
                    'OrderDate = Now
                    'OrderDateParam = Format(OrderDate, "MMYYHHNNSS")
                    
                    TmpRequest = "<x:Envelope xmlns:x=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ws=""http://ws.novared.com.ve"">" & _
                        "<x:Header/>" & _
                        "<x:Body>" & _
                            "<ws:confirmCloseout>" & _
                                "<ws:userCompany>" & Company_ID & "</ws:userCompany>" & _
                                "<ws:user>" & Company_UserID & "</ws:user>" & _
                                "<ws:login>" & Company_User & "</ws:login>" & _
                                "<ws:posId>" & Company_POS_ID & "</ws:posId>" & _
                                "<ws:securityToken>" & TmpSecurityToken & "</ws:securityToken>" & _
                            "</ws:confirmCloseout>" & _
                        "</x:Body>" & _
                    "</x:Envelope>"
                    
                    'TmpSOAPUrl = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService"
                    'TmpSOAPAction = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService/confirmCloseout"
    
                    TmpSOAPUrl = ServiceURL
                    TmpSOAPAction = ServiceURL & "/confirmCloseout"
                    
                    If DebugMode Then
                        Mensaje True, TmpRequest
                        Mensaje True, TmpSOAPAction
                    End If
                    
                    If DebugMode Then
                        Mensaje True, "Instanciando Objetos DOMDocument60, MSXML2.ServerXMLHTTP"
                    End If
                    
                    Set RequestDocument = New DOMDocument60
                    Set CloseOutQuery = New MSXML2.ServerXMLHTTP
                    
                    If DebugMode Then
                        Mensaje True, "Procediendo a cargar Request en el RequestDocument"
                    End If
                    
                    RequestDocument.async = False
                    RequestDocument.loadXML TmpRequest
                    
                    CloseOutQuery.open "POST", TmpSOAPUrl, False
                    
                    CloseOutQuery.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
                    CloseOutQuery.setRequestHeader "SOAPAction", TmpSOAPAction
                    
                    DoEvents
                    
                    If DebugMode Then
                        Mensaje True, "Enviando RequestDocument: " & vbNewLine & vbNewLine & _
                        RequestDocument.xml
                    End If
                    
                    CloseOutQuery.send RequestDocument.xml
                    
                    DoEvents
                    
                    TmpResponse = CloseOutQuery.responseText
                    
                    Set ResponseDocument = New DOMDocument60
                    
                    If DebugMode Then
                        Mensaje True, "Raw Response: " & vbNewLine & TmpResponse & vbNewLine & vbNewLine & _
                        "Intentando cargar ResponseDocument"
                    End If
                    
                    If (ResponseDocument.loadXML(TmpResponse) And Trim(TmpResponse) <> vbNullString) Then ' Respuesta de Solicitud.
                        
                        If DebugMode Then
                            Mensaje True, "ResponseDocument cargado. Procediendo a habilitar Namespaces y obtener ns1:executeCloseoutReturn"
                        End If
                        
                        ResponseDocument.setProperty "SelectionLanguage", "XPath"
                        
                        ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:ns1='http://ws.novared.com.ve'"
                        
                        Set confirmCloseoutResponse = ResponseDocument.selectSingleNode("/soapenv:Envelope/soapenv:Body/ns1:confirmCloseoutResponse")
                        
                        ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
                        
                        Set confirmCloseoutReturn = confirmCloseoutResponse.selectSingleNode("ns1:confirmCloseoutReturn")
                        
                        Set Result = New DOMDocument60
                        
                        Result.setProperty "SelectionLanguage", "XPath"
                        
                        If DebugMode Then
                            Mensaje True, "confirmCloseoutReturn.Text:" & vbNewLine & confirmCloseoutReturn.Text
                        End If
                        
                        If Result.loadXML(confirmCloseoutReturn.Text) Then
                            
                            If DebugMode Then
                                Mensaje True, "Result Cargado. Evaluando respuesta: " & _
                                Result.xml
                            End If
                            
                            TmpResponseCode = Result.selectSingleNode("result/response_code").Text
                            TmpMessage = Result.selectSingleNode("result/user_message").Text
                            
                            If TmpResponseCode = "00" Then
                                
                                TmpLogCode = Result.selectSingleNode("result/log_code").Text
                                TmpUnconfirmedSerials = Result.selectSingleNode("result/unchanged_set").Text
                                
                                Success_Step2 = True
                                
                            Else
                                Mensaje True, "No se pudo realizar el cierre. Informaci�n adicional: " & GetLines & TmpMessage
                            End If
                            
                        Else
                            
                            Err.Raise 1
                            
ErrorConfirmacion:
                            
                            If Err.Number <> 0 Then
                                TmpMessage = "No se recibi� respuesta por parte del proveedor de servicios." & _
                                "El cierre no fue confirmado. " & _
                                IIf(Err.Description <> vbNullString, "Informaci�n t�cnica adicional: " & Err.Description, vbNullString)
                            End If
                            
                            If DebugMode Then Mensaje True, TmpMessage
                            
                        End If
                        
                    Else
                        Err.Raise 1
                    End If
                        
                    If Success_Step1 Then ' Parece que el Confirm no esta sirviendo y como que no es muy importante...
                        
                        If DebugMode Then
                            Mensaje True, "Success. Devolviendo par�metros a DLLResponseDataObject: " & _
                            Result.xml
                        End If
                        
                        Set DLLResponseDataObject = New Collection
                        
                        Collection_AddKey DLLResponseDataObject, "Cierre de Turno", "DLLAction"
                        Collection_AddKey DLLResponseDataObject, TmpReceipt, "Receipt"
                        
                        Collection_AddKey DLLResponseDataObject, TmpLogCode, "LogCode"
                        Collection_AddKey DLLResponseDataObject, TmpTotalTransactions, "TotalTransactions"
                        Collection_AddKey DLLResponseDataObject, TmpTotalAmount, "TotalAmount"
                        Collection_AddKey DLLResponseDataObject, TmpUnconfirmedSerials, "TmpUnconfirmedSerials"
                        
                        Collection_AddKey DLLResponseDataObject, OrderDate, "OrderDate"
                        Collection_AddKey DLLResponseDataObject, OrderDateParam, "OrderDateSentString"
                        Collection_AddKey DLLResponseDataObject, EncodeISOFullDate(OrderDate), "OrderDateISO"
                        
                        InputParams("f_Punto").NOVARED_EjecutarCierreOperaciones_CallBack DLLResponseDataObject
                        
                        Mensaje True, "Cierre realizado exitosamente."
                        
                        ForcedExit
                        
                    End If
                    
                End If
                
            Else
                Mensaje True, "No se puede realizar el Cierre de Operaciones de Servicios si el sistema esta fuera de l�nea."
            End If
            
        End If
        
    End If
    
End Sub

Private Sub CmdClose_Click()
    Unload Me
End Sub

Private Sub LoadPhoneServiceType(pProductTypeCode As PhoneServices_ProductType)
    'Me.Visible = False
    FrmPhoneProducts.ProductType = pProductTypeCode
    FrmPhoneProducts.Show vbModal
    Set FrmPhoneProducts = Nothing
    'Me.Visible = True
End Sub

Private Sub CrearMatrizDeBotones(ByVal ButtonPanelItems As Integer)
    
    On Error GoTo Error
    
    Dim ButtonPanel As Collection
    
    If Not ButtonMatrix Is Nothing Then Exit Sub
    
    Set ButtonMatrix = New Collection
    
    i = 1
    
    While Not i > ImgProduct.UBound
        
        DoEvents
        
        If ButtonPanel Is Nothing Then Set ButtonPanel = New Collection
        
        ButtonPanel.Add i
        
        If (i Mod ButtonPanelItems) = 0 Or i = ImgProduct.UBound Then ButtonMatrix.Add ButtonPanel: Set ButtonPanel = Nothing
        
        i = i + 1
        
    Wend
    
    CmdBack.Visible = True
    CmdBack.Enabled = False
    CmdBack.ZOrder 0
    
    CmdNext.Visible = True
    CmdNext.Enabled = True
    CmdNext.ZOrder 0
    
    CurrentButtonPanelIndex = 1
    
    Exit Sub
    
Error:
    
    Debug.Print Err.Description
    
End Sub

Private Sub OrganizarBotones(Optional SetPanelIndex As Boolean = False)
    
    'MousePointer = vbHourglass
    
    Dim StartIndex As Integer, EndIndex As Integer
    
    If SetPanelIndex Then
        StartIndex = ButtonMatrix(CurrentButtonPanelIndex)(1)
        EndIndex = ButtonMatrix(CurrentButtonPanelIndex)(ButtonMatrix(CurrentButtonPanelIndex).Count)
    Else
        StartIndex = 1
        EndIndex = ImgProduct.UBound
    End If
    
    TmpRowHeight = 9360
    TmpColWidth = InitialButtonPanelMargin - ButtonLeftMargin
    
    For i = StartIndex To EndIndex
        'DoEvents
        TmpColWidth = AddButtonToRow(i, TmpColWidth)
        If (TmpRowHeight + ButtonHeight) > Me.Height Then
            CrearMatrizDeBotones i - 1
            Exit For
        End If
    Next i
    
    MousePointer = MousePointerConstants.vbDefault
    
End Sub

Private Function AddButtonToRow(ByVal pIndex As Integer, ByRef pPosicionAnterior As Long) As Long
    
    If (pPosicionAnterior + ButtonLeftMargin + ButtonWidth) > CmdNext.Left Then
        TmpRowHeight = TmpRowHeight + ButtonTopMargin + ButtonHeight
        pPosicionAnterior = InitialButtonPanelMargin - ButtonLeftMargin
    End If
    
    If (TmpRowHeight + ButtonHeight) > Me.Height Then AddButtonToRow = pPosicionAnterior: Exit Function
    
    LoadFileToControl PicLoad, FindPath(FilteredData(ImgProduct(pIndex).Tag)("Code") & "\" & "ProductImage.png", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, App.Path & "\Images")
    
    If PicLoad.Picture.Handle = 0 Then
        PicLoad.Picture = FrmOtherResources.ImgNotFound.Picture
    End If
    
    Set ImgProduct(pIndex).Picture = PicLoad.Picture
    
    ImgProduct(pIndex).Top = TmpRowHeight
    
    ImgProduct(pIndex).Left = ButtonLeftMargin + pPosicionAnterior
    
    ImgProduct(pIndex).Height = ButtonHeight
    
    ImgProduct(pIndex).Width = ButtonWidth
    
    ImgProduct(pIndex).Visible = True
        
    AddButtonToRow = ImgProduct(pIndex).Left + ImgProduct(pIndex).Width
    
End Function

Private Sub PreviousButtonPanel()
    If CurrentButtonPanelIndex > 1 Then
        CurrentButtonPanelIndex = CurrentButtonPanelIndex - 1
        If CurrentButtonPanelIndex = 1 Then CmdBack.Enabled = False
        DisplayButtonPanel
        CmdNext.Enabled = True
    End If
End Sub

Private Sub NextButtonPanel()
    If CurrentButtonPanelIndex < ButtonMatrix.Count Then
        CurrentButtonPanelIndex = CurrentButtonPanelIndex + 1
        If CurrentButtonPanelIndex = ButtonMatrix.Count Then CmdNext.Enabled = False
        DisplayButtonPanel
        CmdBack.Enabled = True
    End If
End Sub

Private Sub DisplayButtonPanel()

    For i = ImgProduct.LBound To ImgProduct.UBound
        ImgProduct(i).Visible = False
    Next i
    
    OrganizarBotones True

End Sub

Private Sub CmdNext_Click()
    NextButtonPanel
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            If Not WebDoc.Visible Then
                CmdClose_Click
            Else
                CancelLongRunningOperation = True
            End If
    End Select
End Sub

Private Sub Form_Load()
    
    CancelLongRunningOperation = False
    TimerKeepAnimation.Enabled = False
    
    If Not ServiceReady Then
        Mensaje True, "El servicio no est� disponible por los momentos. Por favor verifique su conexi�n a Internet, la disponibilidad del servidor de recargas y asegurese de que actualmente est� operativo en el manejo de solicitudes externas."
        Exit Sub
    End If
    
    InitialButtonPanelMargin = 1550
        
    ButtonTopMargin = 9360
    ButtonLeftMargin = 315
    ButtonWidth = 2835
    ButtonHeight = 1755
    
    CmdBack.Top = ButtonTopMargin + Fix((ButtonHeight / 2) - (CmdBack.Height / 2))
    CmdNext.Top = CmdBack.Top
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub Form_Activate()
    
If Not FormLoaded Then
        
    FormLoaded = True
    
    If Not ServiceReady Then
        Unload Me
        Exit Sub
    End If
    
    ' --------------------------
    
    On Error GoTo 0
    
    ' --------------------------
    
    Dim ProductData As Object, Item As Object, TmpVar As Variant, i As Integer
    
    Set AllProductsData = New Collection
    
    For Each ProductData In TmpCarrierList.childNodes
        
        Set Item = New Dictionary
        
        Item("Code") = ProductData.selectSingleNode("company_id").Text
        Item("ProductCode") = Item("Code")
        Item("Operation_Rules") = ProductData.selectSingleNode("company_rules").Text
        Item("ProductTypeID") = TypeGeneralTopup
        
        Select Case True
            Case (Item("Code") = "1")
                Item("CarrierName") = "DirecTV"
                Item("MainParameterName") = "N�mero de Suscripci�n"
            Case (Item("Code") = "219")
                Item("CarrierName") = "Cantv / Movilnet"
                Item("MainParameterName") = "N�mero de Tel�fono"
            Case (Item("Code") = "358")
                Item("CarrierName") = "Digitel"
                Item("MainParameterName") = "N�mero de Tel�fono"
            Case (Item("Code") = "401")
                Item("CarrierName") = "Movistar"
                Item("MainParameterName") = "N�mero de Tel�fono"
            Case NOVARED_EsProductoEstacionamiento(Item("Code"))
                Item("CarrierName") = InputParams("ProductosEstacionamiento")(Item("Code"))
                Item("MainParameterName") = "N�mero de Ticket"
            Case Else
                GoTo Ignore
        End Select
        
        Set TmpDenominationList = ProductData.selectSingleNode("company_denom_list")
        
        If Not TmpDenominationList Is Nothing Then
            
            Item("FixedDenominationList") = True
            
            Dim DenominationList As Collection
            Set DenominationList = New Collection
        
            For Each TmpDenomination In TmpDenominationList.childNodes
                DenominationList.Add TmpDenomination.Text
            Next
            
            Set Item("DenominationList") = DenominationList
            
        Else
            
            Item("FixedDenominationList") = False
            
        End If
        
        Item("MinAmount") = Null
        Item("MaxAmount") = Null
        
        AllProductsData.Add Item
        
Ignore:
        
    Next
    
    ' --------------------------
    
    Set FilteredData = New Collection
    
    For Each TmpVar In AllProductsData
        
        Set Item = TmpVar
        
        Collection_AddKey FilteredData, Item, Item("Code")
        
    Next
    
    Set TopSoldProductsData = AllProductsData
    
    ' --------------------------
    
    Dim ChangeGeneralImage As Variant
    
    ChangeGeneralImage = App.Path & "\BienvenidaRecargas.png"
    
    If PathExists(ChangeGeneralImage) Then
        
        LoadFileToControl PicLoad, ChangeGeneralImage
        
        If PicLoad.Picture.Handle <> 0 Then
            Set ImgGeneralTopup.Picture = PicLoad.Picture
        End If
        
    Else
        
        ChangeGeneralImage = App.Path & "\BienvenidaRecargas.jpg"
        
        If PathExists(ChangeGeneralImage) Then
            
            Call SafeLoadPicture(PicLoad, ChangeGeneralImage)
            
            If PicLoad.Picture.Handle <> 0 Then
                Set ImgGeneralTopup.Picture = PicLoad.Picture
            End If
            
        End If
        
    End If
    
    ' --------------------------
    
    LimpiarBotones
    
    i = 0
    
    For Each Item In FilteredData
    
        i = i + 1
        
        Load ImgProduct(i)
        ImgProduct(i).Tag = Item("Code")
    
    Next
    
    OrganizarBotones
    
End If
    
End Sub

Private Function ProductBelongsInTopSold(ByVal ProductCode As String) As Boolean

    For Each Item In TopSoldProductsData
        If UCase(Item("ProductCode")) = UCase(ProductCode) Then
            ProductBelongsInTopSold = True
            Exit Function
        End If
    Next

End Function

Private Sub LimpiarBotones()

    On Error Resume Next

    For i = 1 To ImgProduct.UBound
        Unload ImgProduct(i)
    Next i
    
    CmdBack.Visible = False: CmdNext.Visible = False
    
End Sub

Private Sub ImgGeneralTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeGeneralTopup
End Sub

Private Sub ImgProduct_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        
        Set SelectedProductData = FilteredData(ImgProduct(Index).Tag)
        'MsgBox SelectedProductData("CarrierName")
                
        Set FrmPhoneProducts_Order.ProductData = SelectedProductData
        Set FrmPhoneProducts_Order.ProductImage = ImgProduct(Index).Picture
        FrmPhoneProducts_Order.ProductType = SelectedProductData("ProductTypeID")
        FrmPhoneProducts_Order.Show vbModal
        Set FrmPhoneProducts_Order = Nothing
        
    Else
        ' Let Right Click automatically display Tooltip...
    End If
End Sub

Private Sub TimerKeepAnimation_Timer()
    DoEvents
End Sub
