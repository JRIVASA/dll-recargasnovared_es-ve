VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RichTx32.ocx"
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form FrmPhoneProducts_Order 
   BorderStyle     =   0  'None
   ClientHeight    =   11520
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11490
   ScaleMode       =   0  'User
   ScaleWidth      =   15330
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   585
      Left            =   0
      TabIndex        =   84
      Top             =   684
      Width           =   15330
   End
   Begin VB.Frame FrameSummary 
      BackColor       =   &H00E7E8E8&
      Height          =   5055
      Left            =   4320
      TabIndex        =   72
      Top             =   2880
      Visible         =   0   'False
      Width           =   10740
      Begin VB.Frame FrameContinue2 
         BackColor       =   &H00E7E8E8&
         Height          =   2355
         Left            =   6000
         TabIndex        =   73
         Top             =   1395
         Width           =   3840
         Begin SHDocVwCtl.WebBrowser WebDoc 
            Height          =   315
            Left            =   840
            TabIndex        =   83
            Top             =   1920
            Visible         =   0   'False
            Width           =   960
            ExtentX         =   1693
            ExtentY         =   556
            ViewMode        =   0
            Offline         =   0
            Silent          =   0
            RegisterAsBrowser=   0
            RegisterAsDropTarget=   1
            AutoArrange     =   0   'False
            NoClientEdge    =   0   'False
            AlignLeft       =   0   'False
            NoWebView       =   0   'False
            HideFileNames   =   0   'False
            SingleClick     =   0   'False
            SingleSelection =   0   'False
            NoFolders       =   0   'False
            Transparent     =   0   'False
            ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
            Location        =   ""
         End
         Begin VB.Frame FrameConfirm 
            BackColor       =   &H001BC957&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1185
            Left            =   720
            TabIndex        =   74
            Top             =   600
            Width           =   2355
            Begin VB.Label lblConfirm 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Confirmar"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   24
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   720
               Left            =   0
               TabIndex        =   75
               Top             =   275
               Width           =   2355
            End
         End
      End
      Begin VB.Label lblTotalAmount 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "11.00 $"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   480
         Left            =   240
         TabIndex        =   82
         Top             =   4200
         Width           =   6105
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Total a Pagar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   240
         TabIndex        =   81
         Top             =   3600
         Width           =   6105
      End
      Begin VB.Label lblTotalFees 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "1.00 $"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   480
         Left            =   240
         TabIndex        =   80
         Top             =   3000
         Width           =   6105
      End
      Begin VB.Label lblFees 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Impuesto (s)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   240
         TabIndex        =   79
         Top             =   2400
         Width           =   6105
      End
      Begin VB.Label lblOrderSummary 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Resumen de la Orden"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   26.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   600
         Left            =   240
         TabIndex        =   78
         Top             =   360
         Width           =   6225
      End
      Begin VB.Label lblSelectedProductName 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "RECARGA MOVISTAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   240
         TabIndex        =   77
         Top             =   1800
         Width           =   6105
      End
      Begin VB.Label lblProductName 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre del Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   240
         TabIndex        =   76
         Top             =   1200
         Width           =   6105
      End
   End
   Begin VB.Frame FrameViewAccessNumbers 
      BackColor       =   &H00E7E8E8&
      Height          =   618
      Left            =   9720
      TabIndex        =   70
      Top             =   10200
      Visible         =   0   'False
      Width           =   3810
      Begin RichTextLib.RichTextBox txtAccessNumbers 
         Height          =   405
         Left            =   120
         TabIndex        =   71
         Top             =   120
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   714
         _Version        =   393217
         BackColor       =   15198440
         BorderStyle     =   0
         ScrollBars      =   2
         Appearance      =   0
         TextRTF         =   $"FrmPhoneProducts_Order.frx":0000
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame FrameViewRates 
      BackColor       =   &H00E7E8E8&
      Height          =   618
      Left            =   7560
      TabIndex        =   68
      Top             =   10200
      Visible         =   0   'False
      Width           =   2010
      Begin RichTextLib.RichTextBox txtRates 
         Height          =   405
         Left            =   120
         TabIndex        =   69
         Top             =   120
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   714
         _Version        =   393217
         BackColor       =   15198440
         BorderStyle     =   0
         ScrollBars      =   2
         Appearance      =   0
         TextRTF         =   $"FrmPhoneProducts_Order.frx":0086
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame FrameViewToS 
      BackColor       =   &H00E7E8E8&
      Height          =   618
      Left            =   4200
      TabIndex        =   66
      Top             =   10200
      Visible         =   0   'False
      Width           =   3210
      Begin RichTextLib.RichTextBox txtTermsOfUse 
         Height          =   405
         Left            =   120
         TabIndex        =   67
         Top             =   120
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   714
         _Version        =   393217
         BackColor       =   15198440
         BorderStyle     =   0
         ScrollBars      =   2
         Appearance      =   0
         TextRTF         =   $"FrmPhoneProducts_Order.frx":0103
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox PicInfo 
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   120
      Picture         =   "FrmPhoneProducts_Order.frx":0187
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   65
      Top             =   10320
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.PictureBox PicBack 
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   720
      Picture         =   "FrmPhoneProducts_Order.frx":0E51
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   64
      Top             =   10320
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Frame FrameStellarBar 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   690
      Left            =   0
      TabIndex        =   50
      Top             =   0
      Width           =   15675
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   795
         Left            =   12675
         Picture         =   "FrmPhoneProducts_Order.frx":479E
         ScaleHeight     =   795
         ScaleWidth      =   2415
         TabIndex        =   51
         Top             =   -50
         Width           =   2415
      End
      Begin VB.Label LbWebsite 
         BackColor       =   &H00404040&
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   52
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame FrameMainView 
      Height          =   9075
      Left            =   4459
      TabIndex        =   4
      Top             =   1504
      Width           =   10740
      Begin VB.Frame FrameContinue 
         BackColor       =   &H00E7E8E8&
         Height          =   2355
         Left            =   6840
         TabIndex        =   59
         Top             =   6600
         Width           =   3840
         Begin VB.Frame FrameProceed 
            BackColor       =   &H001BC957&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1185
            Left            =   720
            TabIndex        =   60
            Top             =   600
            Width           =   2355
            Begin VB.Label lblProceed 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Proceder"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   24
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   720
               Left            =   0
               TabIndex        =   61
               Top             =   275
               Width           =   2355
            End
         End
      End
      Begin VB.Frame FrameAmountOptions 
         BackColor       =   &H00E7E8E8&
         Height          =   4575
         Left            =   120
         TabIndex        =   55
         Top             =   1920
         Width           =   6600
         Begin VB.Frame FrameDenomination 
            BackColor       =   &H8000000D&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   825
            Index           =   0
            Left            =   250
            TabIndex        =   56
            Top             =   1200
            Visible         =   0   'False
            Width           =   1395
            Begin VB.Label lblDenomination 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "250.00 $"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   15.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   480
               Index           =   0
               Left            =   0
               TabIndex        =   57
               Top             =   225
               Width           =   1395
            End
         End
         Begin VB.Label lblSelectAmount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Select Amount"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   240
            TabIndex        =   58
            Top             =   390
            Width           =   6105
         End
      End
      Begin VB.Frame FrameMinMax 
         BackColor       =   &H00E7E8E8&
         Height          =   690
         Left            =   120
         TabIndex        =   53
         Top             =   6575
         Width           =   6600
         Begin VB.Label lblMinMax 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Min: 0.00 / Max: 250.00"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   240
            TabIndex        =   54
            Top             =   120
            Width           =   6100
         End
      End
      Begin VB.Frame FramePhoneNumber 
         BackColor       =   &H00E7E8E8&
         Height          =   1575
         Left            =   120
         TabIndex        =   18
         Top             =   255
         Width           =   6600
         Begin VB.TextBox TxtPhoneNumber 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   630
            Left            =   240
            MaxLength       =   50
            TabIndex        =   19
            Text            =   "(+58) 0261-555-0000"
            Top             =   780
            Width           =   6100
         End
         Begin VB.Label lblPhoneNumber 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Introduzca N�mero de Telefono"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   240
            TabIndex        =   20
            Top             =   240
            Width           =   6100
         End
      End
      Begin VB.Frame FramePaymentInfo 
         BackColor       =   &H00E7E8E8&
         Height          =   1657
         Left            =   120
         TabIndex        =   15
         Top             =   7320
         Width           =   6600
         Begin VB.TextBox TxtAmount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   630
            Left            =   240
            MaxLength       =   50
            TabIndex        =   16
            Text            =   "250"
            Top             =   840
            Width           =   6100
         End
         Begin VB.Label lblAmount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Monto a Pagar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   240
            TabIndex        =   17
            Top             =   240
            Width           =   6100
         End
      End
      Begin VB.Frame FrameKeyboard 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Height          =   6255
         Left            =   6840
         TabIndex        =   14
         Top             =   240
         Width           =   3840
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   14
            Left            =   2640
            TabIndex        =   49
            Top             =   5040
            Width           =   1005
            Begin VB.Image PicTecladoKey 
               Height          =   480
               Index           =   14
               Left            =   240
               Picture         =   "FrmPhoneProducts_Order.frx":6DD4
               Stretch         =   -1  'True
               Top             =   240
               Width           =   480
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   13
            Left            =   1440
            TabIndex        =   48
            Top             =   5040
            Width           =   1005
            Begin VB.Image PicTecladoKey 
               Height          =   480
               Index           =   0
               Left            =   240
               Picture         =   "FrmPhoneProducts_Order.frx":7616
               Stretch         =   -1  'True
               Top             =   240
               Width           =   480
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   12
            Left            =   240
            TabIndex        =   47
            Top             =   5040
            Width           =   1005
            Begin VB.Image PicTecladoKey 
               Height          =   480
               Index           =   12
               Left            =   240
               Picture         =   "FrmPhoneProducts_Order.frx":7E58
               Stretch         =   -1  'True
               Top             =   240
               Width           =   480
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   11
            Left            =   2640
            TabIndex        =   45
            Top             =   3840
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "-"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   27.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   11
               Left            =   0
               TabIndex        =   46
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   0
            Left            =   1440
            TabIndex        =   43
            Top             =   3840
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   0
               Left            =   0
               TabIndex        =   44
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   10
            Left            =   240
            TabIndex        =   41
            Top             =   3840
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "."
               BeginProperty Font 
                  Name            =   "Comic Sans MS"
                  Size            =   72
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   1995
               Index           =   10
               Left            =   0
               TabIndex        =   42
               Top             =   -990
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   9
            Left            =   2640
            TabIndex        =   39
            Top             =   2640
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "9"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   9
               Left            =   0
               TabIndex        =   40
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   8
            Left            =   1440
            TabIndex        =   37
            Top             =   2640
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "8"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   8
               Left            =   0
               TabIndex        =   38
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   7
            Left            =   240
            TabIndex        =   35
            Top             =   2640
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "7"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   7
               Left            =   0
               TabIndex        =   36
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   6
            Left            =   2640
            TabIndex        =   33
            Top             =   1440
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "6"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   6
               Left            =   0
               TabIndex        =   34
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   5
            Left            =   1440
            TabIndex        =   31
            Top             =   1440
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "5"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   5
               Left            =   0
               TabIndex        =   32
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   4
            Left            =   240
            TabIndex        =   29
            Top             =   1440
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "4"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   4
               Left            =   0
               TabIndex        =   30
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   3
            Left            =   2640
            TabIndex        =   27
            Top             =   300
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "3"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   3
               Left            =   0
               TabIndex        =   28
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   2
            Left            =   1440
            TabIndex        =   25
            Top             =   300
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "2"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   2
               Left            =   0
               TabIndex        =   26
               Top             =   90
               Width           =   1005
            End
         End
         Begin VB.Frame FrameTecladoKey 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   1
            Left            =   240
            TabIndex        =   23
            Top             =   300
            Width           =   1005
            Begin VB.Label lblTecladoKey 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H8000000D&
               BackStyle       =   0  'Transparent
               Caption         =   "1"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   30
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00585A58&
               Height          =   600
               Index           =   1
               Left            =   0
               TabIndex        =   24
               Top             =   90
               Width           =   1005
            End
         End
      End
   End
   Begin VB.Frame FrameViewOptions 
      Height          =   9075
      Left            =   150
      TabIndex        =   3
      Top             =   1504
      Width           =   4140
      Begin VB.Frame FrameGoBack 
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   240
         TabIndex        =   62
         Top             =   7560
         Width           =   3675
         Begin VB.Label lblGoBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Regresar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   24
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   720
            Left            =   120
            TabIndex        =   63
            Top             =   150
            Width           =   3495
         End
      End
      Begin VB.Frame FrameProductInfo 
         BackColor       =   &H00E7E8E8&
         BorderStyle     =   0  'None
         Height          =   2895
         Left            =   165
         TabIndex        =   21
         Top             =   360
         Width           =   3810
         Begin VB.Image ImgProduct 
            Height          =   2115
            Left            =   135
            Stretch         =   -1  'True
            Top             =   705
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.Label lblCarrierName 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "OPERADORA"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   -120
            TabIndex        =   22
            Top             =   120
            Width           =   4095
         End
      End
      Begin VB.Frame FrameAccessNumbers 
         BackColor       =   &H00E7E8E8&
         Height          =   1035
         Left            =   120
         TabIndex        =   11
         Top             =   6120
         Width           =   3925
         Begin VB.PictureBox PicAccessNumbers 
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Height          =   480
            Left            =   120
            Picture         =   "FrmPhoneProducts_Order.frx":869A
            ScaleHeight     =   480
            ScaleWidth      =   480
            TabIndex        =   12
            Top             =   300
            Width           =   480
         End
         Begin VB.Label lblAccessNumbers 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "N�m. de Acceso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   720
            TabIndex        =   13
            Top             =   300
            Width           =   3100
         End
      End
      Begin VB.Frame FrameToS 
         BackColor       =   &H00E7E8E8&
         Height          =   1035
         Left            =   120
         TabIndex        =   8
         Top             =   4800
         Width           =   3925
         Begin VB.PictureBox PicToS 
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Height          =   480
            Left            =   120
            Picture         =   "FrmPhoneProducts_Order.frx":9364
            ScaleHeight     =   480
            ScaleWidth      =   480
            TabIndex        =   9
            Top             =   300
            Width           =   480
         End
         Begin VB.Label lblToS 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "T�rminos de Uso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   720
            TabIndex        =   10
            Top             =   300
            Width           =   3105
         End
      End
      Begin VB.Frame FrameRates 
         BackColor       =   &H00E7E8E8&
         Height          =   1035
         Left            =   120
         TabIndex        =   5
         Top             =   3480
         Width           =   3925
         Begin VB.PictureBox PicRates 
            BackColor       =   &H00E7E8E8&
            BorderStyle     =   0  'None
            Height          =   480
            Left            =   120
            Picture         =   "FrmPhoneProducts_Order.frx":A02E
            ScaleHeight     =   480
            ScaleWidth      =   480
            TabIndex        =   6
            Top             =   300
            Width           =   480
         End
         Begin VB.Label lblRates 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Ver Tarifas"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   20.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   480
            Left            =   720
            TabIndex        =   7
            Top             =   300
            Width           =   3100
         End
      End
   End
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   1560
      ScaleHeight     =   495
      ScaleWidth      =   2415
      TabIndex        =   1
      Top             =   9240
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.TextBox TxtJson 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   1560
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "FrmPhoneProducts_Order.frx":ACF8
      Top             =   10320
      Visible         =   0   'False
      Width           =   2415
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   18720
      TabIndex        =   2
      Top             =   360
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   53
      _Version        =   393216
      BackColor       =   -2147483633
      BackColorSel    =   -2147483633
      BackColorBkg    =   -2147483633
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   690
      Left            =   0
      TabIndex        =   85
      Top             =   10845
      Width           =   15330
   End
End
Attribute VB_Name = "FrmPhoneProducts_Order"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ProductType As PhoneServices_ProductType
Public ProductData As Object
Public ProductImage As Object

Private UIReady As Boolean
Private FormReady As Boolean

Private KBDFocusObj As Object

Private InitialButtonPanelMargin As Long

Private ButtonTopMargin As Long
Private ButtonLeftMargin As Long
Private ButtonHeight As Long
Private ButtonWidth As Long

Private TmpRowHeight As Long
Private TmpColWidth As Long

Public MinAmount As Double
Public MaxAmount As Double

Private SelectedDenominationIndex As Integer

Private Sub CmdExit_Click()
    Unload Me
End Sub

Private Function AddButtonToRow(ByVal pIndex As Integer, ByRef pPosicionAnterior As Long) As Long
    
    If (pPosicionAnterior + ButtonLeftMargin + ButtonWidth) > FrameAmountOptions.Width Then
        TmpRowHeight = TmpRowHeight + ButtonTopMargin + ButtonHeight
        pPosicionAnterior = InitialButtonPanelMargin - ButtonLeftMargin
    End If
    
    FrameDenomination(pIndex).Top = TmpRowHeight
    
    FrameDenomination(pIndex).Left = ButtonLeftMargin + pPosicionAnterior
    
    FrameDenomination(pIndex).Height = ButtonHeight
    
    FrameDenomination(pIndex).Width = ButtonWidth
    
    FrameDenomination(pIndex).Visible = True
        
    AddButtonToRow = FrameDenomination(pIndex).Left + FrameDenomination(pIndex).Width
    
End Function

Private Sub Form_Activate()
    
    If Not UIReady Then
        
        DoEvents
        
        If Not FormReady Then
            Mensaje True, "Hubo un problema al cargar la configuraci�n. Por favor int�ntelo de nuevo mas tarde."
            Unload Me
            Exit Sub
        End If
        
        UIReady = True
        
        If FrameDenomination.UBound = 1 Then
            lblDenomination_MouseUp 1, vbLeftButton, 0, 0, 0
        End If
        
        If PuedeObtenerFoco(TxtPhoneNumber) Then
            TxtPhoneNumber.SetFocus
        ElseIf PuedeObtenerFoco(TxtAmount) Then
            TxtAmount.SetFocus
        End If
        
    End If
    
End Sub

Private Sub Form_Load()
    
    On Error GoTo ErrLoad
    
    Dim ShowAmountOptions As Boolean, MinAmt As String, MaxAmt As String
    
    lblCarrierName.Caption = ProductData("CarrierName")
    If Not ProductImage Is Nothing Then Set ImgProduct.Picture = ProductImage
    ImgProduct.ToolTipText = lblCarrierName.Caption
    ImgProduct.Visible = True
    
    lblPhoneNumber.Font.Size = 16
    lblPhoneNumber.Caption = "Introduzca el " & ProductData("MainParameterName")
    
    If NOVARED_EsProductoEstacionamiento(ProductData("Code")) Then
        TxtPhoneNumber.Enabled = False
        TxtPhoneNumber.Text = Val(ProductData("Code"))
    Else
        TxtPhoneNumber.Enabled = True 'IIf(IsNull(ProductData("AskPhoneNumber")), True, CBool(ProductData("AskPhoneNumber")))
        TxtPhoneNumber.Text = vbNullString
    End If
    
    If Not TxtPhoneNumber.Enabled Then
        lblPhoneNumber.ForeColor = -2147483636 ' SystemColorConstants.vbApplicationWorkspace
        TxtPhoneNumber.BackColor = -2147483636 'SystemColorConstants.vbApplicationWorkspace
    End If
    
    ProductData("MinAmount") = IIf(IsNull(ProductData("MinAmount")), "", ProductData("MinAmount"))
    ProductData("MaxAmount") = IIf(IsNull(ProductData("MaxAmount")), "", ProductData("MaxAmount"))
    
    MinAmt = IIf(Val(ProductData("MinAmount")) <= 0, "None", ProductData("MinAmount"))
    MaxAmt = IIf(Val(ProductData("MaxAmount")) <= 0, "None", ProductData("MaxAmount"))
    
    If Val(MinAmt) <= 0 And Val(MaxAmt) <= 0 Then
        lblMinMax.Caption = "Min / Max: " & "Consulte los T�rminos."
    Else
        lblMinMax.Caption = "Min: " & MinAmt & " | Max: " & MaxAmt
    End If
    
    MinAmount = Val(MinAmt)
    MaxAmount = Val(MaxAmt)
    
    If ProductData("FixedDenominationList") Then
        ShowAmountOptions = True
        lblSelectAmount.Caption = "Seleccione el Monto"
    End If
    
    TxtAmount.Text = vbNullString
    
    FrameRates.Visible = False
    FrameAccessNumbers.Visible = False
    
    OptionalViewResetIcons
    
    With FrameViewToS
        Set .Container = Me
        .Left = FrameMainView.Left - 20
        .Top = FrameMainView.Top
        .Width = FrameMainView.Width
        .Height = FrameMainView.Height
    End With
    
    With txtTermsOfUse
        
        '.BackColor = vbRed ' Debug
        .Left = 250
        .Top = 250
        .Width = FrameViewToS.Width - 600
        .Height = FrameViewToS.Height - 900
        
        '.TextRTF = IIf(IsNull(ProductData("Disclaimer")), vbNullString, ProductData("Disclaimer"))
        
        TmpDisplayText = IIf(IsNull(ProductData("Operation_Rules")), vbNullString, ProductData("Operation_Rules"))
        
        If Len(TmpDisplayText) > 0 Then
            TmpDisplayText = UCase(Mid(TmpDisplayText, 1, 1)) & Mid(TmpDisplayText, 2)
            If Mid(TmpDisplayText, Len(TmpDisplayText), 1) <> "." Then
                TmpDisplayText = TmpDisplayText & "."
            End If
        End If
        
        .TextRTF = TmpDisplayText
        
        .SelStart = 0
        .SelLength = Len(.TextRTF)
        .SelColor = lblCarrierName.ForeColor
        '.SelAlignment = rtfCenter
        
    End With
    
    With FrameViewRates
        Set .Container = Me
        .Left = FrameMainView.Left - 20
        .Top = FrameMainView.Top
        .Width = FrameMainView.Width
        .Height = FrameMainView.Height
    End With
    
    With txtRates
        
        '.BackColor = vbRed ' Debug
        .Left = 250
        .Top = 250
        .Width = FrameViewRates.Width - 600
        .Height = FrameViewRates.Height - 900
        
        .TextRTF = IIf(IsNull(Null), vbNullString, vbNullString) ' Determinar que informaci�n va aqu�.
        .SelStart = 0
        .SelLength = Len(.TextRTF)
        .SelColor = lblCarrierName.ForeColor
        '.SelAlignment = rtfCenter
        
    End With
    
    With FrameViewAccessNumbers
        Set .Container = Me
        .Left = FrameMainView.Left - 20
        .Top = FrameMainView.Top
        .Width = FrameMainView.Width
        .Height = FrameMainView.Height
    End With
    
    With txtAccessNumbers
        
        '.BackColor = vbRed ' Debug
        .Left = 250
        .Top = 250
        .Width = FrameViewAccessNumbers.Width - 600
        .Height = FrameViewAccessNumbers.Height - 900
        
        .TextRTF = IIf(IsNull(Null), vbNullString, vbNullString) ' Determinar que informaci�n va aqu�.
        .SelStart = 0
        .SelLength = Len(.TextRTF)
        .SelColor = lblCarrierName.ForeColor
        '.SelAlignment = rtfCenter
        
    End With
    
    If ShowAmountOptions Then
        
        Dim i As Integer
        i = 0
        
        For Each TmpItem In ProductData("DenominationList")
            
            If Not IsNull(TmpItem) Then
                If Not (UCase(TmpItem) Like UCase("*(Agotado)*")) Then
                    
                    i = i + 1
                    
                    Load FrameDenomination(i)
                    Load lblDenomination(i)
                    
                    Set lblDenomination(i).Container = FrameDenomination(i)
                    
                    lblDenomination(i).Left = 0
                    lblDenomination(i).Width = FrameDenomination(i).Width
                    lblDenomination(i).Top = 225
                    lblDenomination(i).Height = 480
                    'lblDenomination(i).Caption = IIf(IsNull(TmpItem), "0.00 Bs.", Val(TmpItem) & " Bs.")
                    'lblDenomination(i).Caption = IIf(IsNull(TmpItem), "0.00 " & InputParams("Moneda_Simbolo"), _
                    Val(TmpItem) & " " & InputParams("Moneda_Simbolo"))
                    ' Mejor no mostrar s�mbolo en cada cuadrito, arruina la visualizaci�n y confunde
                    lblDenomination(i).Caption = IIf(IsNull(TmpItem), "0.00", _
                    Val(TmpItem))
                    lblDenomination(i).Visible = True
                    
                End If
            End If
            
        Next
                
        InitialButtonPanelMargin = 250
            
        ButtonTopMargin = 255
        ButtonLeftMargin = 155
        
        If NOVARED_EsProductoEstacionamiento(ProductData("Code")) Then
            ButtonWidth = 2000
        Else
            ButtonWidth = 1395
        End If
        
        ButtonHeight = 825
        
        TmpRowHeight = 1200
        TmpColWidth = InitialButtonPanelMargin - ButtonLeftMargin
    
        For i = 1 To ValidarNumeroIntervalo(FrameDenomination.UBound, 12) ' Max
            'DoEvents
            TmpColWidth = AddButtonToRow(i, TmpColWidth)
            lblDenomination(i).Width = ButtonWidth
        Next i
        
        TxtAmount.Locked = True
        
    Else
    
        With FramePhoneNumber
            .Left = 120
            .Top = 255
            .Width = 6585
            .Height = 2400
        End With
        
        With lblPhoneNumber
            .Left = 135
            .Top = 360
            .Width = 6285
            .Height = 480
        End With
        
        With TxtPhoneNumber
            .Left = 135
            .Top = 1260
            .Width = 6285
            .Height = 480
        End With
        
        With FrameMinMax
            .Left = 120
            .Top = 2720
            .Width = 6585
            .Height = 1170
        End With
        
        With lblMinMax
        
            .Top = 375
        
        End With
        
        With FramePaymentInfo
            .Left = 120
            .Top = 4080
            .Width = 6585
            .Height = 2400
        End With
        
        With lblAmount
            .Left = 135
            .Top = 360
            .Width = 6285
            .Height = 480
        End With
        
        With TxtAmount
            .Left = 135
            .Top = 1260
            .Width = 6285
            .Height = 480
        End With
        
        FrameAmountOptions.Visible = False
        
    End If
    
    Call AjustarPantalla(Me)
    
    PrepararSpinner
    
    FormReady = True
    
    Exit Sub
    
ErrLoad:
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    DoEvents
End Sub

Private Sub FrameAccessNumbers_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PicAccessNumbers_MouseUp Button, Shift, x, y
End Sub

Private Sub FrameConfirm_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblConfirm_MouseUp Button, Shift, x, y
End Sub

Private Sub FrameDenomination_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    lblDenomination_MouseUp Index, Button, Shift, x, y
End Sub

Private Sub FrameGoBack_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblGoBack_MouseUp Button, Shift, x, y
End Sub

Private Sub FrameRates_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PicRates_MouseUp Button, Shift, x, y
End Sub

Private Sub FrameTecladoKey_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Select Case Index
        Case 0 To 11
            lblTecladoKey_MouseUp Index, Button, Shift, x, y
        Case 12 To 14
            PicTecladoKey_MouseUp Index, Button, Shift, x, y
    End Select
End Sub

Private Sub FrameToS_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PicToS_MouseUp Button, Shift, x, y
End Sub

Private Sub ImgProduct_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 1 Then
        'Set SelectedProductData = FilteredData(Index)
        'MsgBox SelectedProductData("CarrierName")
    ElseIf Button = 2 Then
        ' Let Right Click automatically display Tooltip...
    End If
End Sub

Private Sub lblAccessNumbers_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PicAccessNumbers_MouseUp Button, Shift, x, y
End Sub

Private Sub PrepararSpinner()
        
    On Error Resume Next
        
    Dim TmpFilePath As String
    Dim TmpHeight
    Dim TmpTop
    Dim TmpWidth
    Dim TmpLeft
    
    TmpHeight = FrameContinue2.Height / Screen.TwipsPerPixelY
    TmpTop = (TmpHeight / 2) - (128 / 2)
    TmpWidth = FrameContinue2.Width / Screen.TwipsPerPixelX
    TmpLeft = (TmpWidth / 2) - (128 / 2)
    
    TmpFilePath = FindPath("Spinner.gif", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH)
    
    WebDoc.Left = -50
    WebDoc.Top = -50
    WebDoc.Height = FrameContinue2.Height + 200
    WebDoc.Width = FrameContinue2.Width + 500
    
    TmpFileContents = "<html><head><body style='overflow: hidden; position: relative'><img style='position: absolute; top: " & TmpTop & "; left: " & TmpLeft & "' src = '" & TmpFilePath & "' /></body></head></html>"
    'TmpFileContents = "<html><head><body><img src = '" & TmpFilePath & "' /></body></head></html>"
           
    'Dim A As New SHDocVw.InternetExplorer
            
    'A.Navigate2 "About:Blank"
    
    'A.Visible = True
    
    Dim TmpDoc As Object
    
    WebDoc.Resizable = False
    WebDoc.Navigate2 "About:Blank"
    
    Set TmpDoc = WebDoc.Document
    
    DoEvents
    
    TmpDoc.open
    
    DoEvents
    
    TmpDoc.Write TmpFileContents
    
    DoEvents
    
    TmpDoc.Close
    
    DoEvents
    
    'WebDoc.Visible = True
    WebDoc.ZOrder 0
    
End Sub

Private Sub lblConfirm_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    WebDoc.Visible = True
    
    DoEvents
    
    On Error GoTo ErrorSolicitud
    
    Dim TotalAmount As Double, TotalFees As Double, BaseAmount As Double, OrderDateParam As String, OrderDate As Date
    Dim OrderStatus As Integer, OrderPlaced As Boolean, OrderConfirmed As Boolean
    Dim IntentosSesion As Long: Const MaxIntentosSesion = 2
    
    IntentosSesion = 1
    
Requery:
    
    OrderDate = Now
    OrderDateParam = Format(OrderDate, "MMYYHHNNSS")
    
    BaseAmount = CDbl(TxtAmount.Text)
    TotalFees = CDbl(lblTotalFees.Tag)
    TotalAmount = CDbl(lblTotalAmount.Tag)
    
    TmpRequest = "<x:Envelope xmlns:x=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ws=""http://ws.novared.com.ve"">" & _
        "<x:Header/>" & _
        "<x:Body>" & _
            "<ws:addEcard>" & _
                "<ws:company>" & ProductData("Code") & "</ws:company>" & _
                "<ws:userCompany>" & Company_ID & "</ws:userCompany>" & _
                "<ws:user>" & Company_UserID & "</ws:user>" & _
                "<ws:securityToken>" & TmpSecurityToken & "</ws:securityToken>" & _
                "<ws:accountNumber>" & TxtPhoneNumber.Text & "</ws:accountNumber>" & _
                "<ws:amount>" & CStr(TotalAmount) & "</ws:amount>" & _
                "<ws:posId>" & Company_POS_ID & "</ws:posId>" & _
                "<ws:serviceTime>" & OrderDateParam & "</ws:serviceTime>" & _
                "<ws:serialsToConfirm>" & "" & "</ws:serialsToConfirm>" & _
            "</ws:addEcard>" & _
        "</x:Body>" & _
    "</x:Envelope>"
    
    'TmpSOAPUrl = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService"
    'TmpSOAPAction = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService/AddEcard"
    
    TmpSOAPUrl = ServiceURL
    TmpSOAPAction = ServiceURL & "/AddEcard"
    
    Dim RequestDocument         As MSXML2.DOMDocument60
    Dim ResponseDocument        As MSXML2.DOMDocument60
    Dim PlaceOrderQuery         As MSXML2.ServerXMLHTTP
    Dim TmpResponse             As String
 
    Set RequestDocument = New DOMDocument60
    Set PlaceOrderQuery = New MSXML2.ServerXMLHTTP

    RequestDocument.async = False
    RequestDocument.loadXML TmpRequest
    
    PlaceOrderQuery.open "POST", TmpSOAPUrl, False
    
    PlaceOrderQuery.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    PlaceOrderQuery.setRequestHeader "SOAPAction", TmpSOAPAction
    
    DoEvents
    
    PlaceOrderQuery.send RequestDocument.xml
    
    DoEvents
    
    TmpResponse = PlaceOrderQuery.responseText
    
    Set ResponseDocument = New DOMDocument60
    
    If (ResponseDocument.loadXML(TmpResponse) And Trim(TmpResponse) <> vbNullString) Then ' Respuesta de Solicitud.
        
        ResponseDocument.setProperty "SelectionLanguage", "XPath"
        
        ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
        
        Set AddECardResponse = ResponseDocument.selectSingleNode("/soapenv:Envelope/soapenv:Body/ns1:addEcardResponse")
        
        ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
        
        Set AddECardReturn = AddECardResponse.selectSingleNode("ns1:addEcardReturn")
        
        Set Result = New DOMDocument60
        
        Result.setProperty "SelectionLanguage", "XPath"
        
        If Result.loadXML(AddECardReturn.Text) Then
            
            TmpResponseCode = Result.selectSingleNode("result/response_code").Text
            TmpMessage = Result.selectSingleNode("result/user_message").Text
            
            If TmpResponseCode = "00" Then
                
                OrderPlaced = True
                
                Select Case ProductData("Code")
                    Case "401"
                        OrderStatus = 0
                    Case Else
                        OrderStatus = 1
                End Select
                
                TmpLogCode = Result.selectSingleNode("result/log_code").Text
                TmpSerial = Result.selectSingleNode("result/serial").Text
                TmpPin = Result.selectSingleNode("result/pin").Text
                TmpCustomerID = Result.selectSingleNode("result/customer_id").Text
                TmpServiceSerial = Result.selectSingleNode("result/service_serial").Text
                TmpServicePin = Result.selectSingleNode("result/service_pin").Text
                TmpServiceLot = Result.selectSingleNode("result/service_lot").Text
                
                ' Iniciar proceso de confirmaci�n
                
                On Error GoTo ErrorConfirmacion
                
                TmpRequest = "<x:Envelope xmlns:x=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ws=""http://ws.novared.com.ve"">" & _
                    "<x:Header/>" & _
                    "<x:Body>" & _
                        "<ws:confirmEcard>" & _
                            "<ws:company>" & ProductData("Code") & "</ws:company>" & _
                            "<ws:userCompany>" & Company_ID & "</ws:userCompany>" & _
                            "<ws:user>" & Company_UserID & "</ws:user>" & _
                            "<ws:securityToken>" & TmpSecurityToken & "</ws:securityToken>" & _
                            "<ws:serial>" & TmpSerial & "</ws:serial>" & _
                            "<ws:amount>" & CStr(TotalAmount) & "</ws:amount>" & _
                            "<ws:posId>" & Company_POS_ID & "</ws:posId>" & _
                            "<ws:logCode>" & TmpLogCode & "</ws:logCode>" & _
                            "<ws:customerId>" & TmpCustomerID & "</ws:customerId>" & _
                        "</ws:confirmEcard>" & _
                    "</x:Body>" & _
                "</x:Envelope>"
                
                'TmpSOAPUrl = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService"
                'TmpSOAPAction = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService/ConfirmEcard"
                
                TmpSOAPUrl = ServiceURL
                TmpSOAPAction = ServiceURL & "/ConfirmEcard"
                
                Set RequestDocument = New DOMDocument60
                Set PlaceOrderQuery = New MSXML2.ServerXMLHTTP
            
                RequestDocument.async = False
                RequestDocument.loadXML TmpRequest
                
                PlaceOrderQuery.open "POST", TmpSOAPUrl, False
                
                PlaceOrderQuery.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
                PlaceOrderQuery.setRequestHeader "SOAPAction", TmpSOAPAction
                
                DoEvents
                
                PlaceOrderQuery.send RequestDocument.xml
                
                DoEvents
                
                TmpResponse = PlaceOrderQuery.responseText
                
                Set ResponseDocument = New DOMDocument60
                
                If (ResponseDocument.loadXML(TmpResponse) And Trim(TmpResponse) <> vbNullString) Then ' Respuesta de Confirmaci�n.
                    
                    ResponseDocument.setProperty "SelectionLanguage", "XPath"
                    
                    ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
                    
                    Set ConfirmECardResponse = ResponseDocument.selectSingleNode("/soapenv:Envelope/soapenv:Body/ns1:confirmEcardResponse")
                    
                    ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
                    
                    Set ConfirmECardReturn = ConfirmECardResponse.selectSingleNode("ns1:confirmEcardReturn")
                    
                    Set Result = New DOMDocument60
                    
                    Result.setProperty "SelectionLanguage", "XPath"
                    
                    If Result.loadXML(ConfirmECardReturn.Text) Then
                        
                        TmpResponseCode = Result.selectSingleNode("/result/response_code").Text
                        TmpMessage = Result.selectSingleNode("/result/user_message").Text
                        
                        OrderConfirmed = (TmpResponseCode = "00")
                        
                        If OrderConfirmed Then
                            
                            TmpLogCode = Result.selectSingleNode("/result/log_code").Text
                            Set TmpAmount = Result.selectSingleNode("/result/amount")
                            If Not TmpAmount Is Nothing Then TmpAmount = TmpAmount.Text Else TmpAmount = TotalAmount
                            TmpSerial = Result.selectSingleNode("/result/serial").Text
                            Set TmpDaysOfProgramming = Result.selectSingleNode("/result/days_of_programming")
                            If Not TmpDaysOfProgramming Is Nothing Then TmpDaysOfProgramming = TmpDaysOfProgramming.Text Else TmpDaysOfProgramming = 0
                            If IsNull(TmpDaysOfProgramming) Then TmpDaysOfProgramming = 0
                            TmpDaysOfProgramming = Val(TmpDaysOfProgramming)
                            
                        End If
                        
                        If OrderConfirmed Then
                            OrderStatus = 1
                            TmpMessage = vbNullString
                        Else
                            
                            Select Case ProductData("Code")
                                
                                Case "401"
                                    
                                    OrderPlaced = False
                                    
                                    TmpMessage = "Su solicitud no pudo ser procesada. Informaci�n adicional: " & GetLines & _
                                    TmpMessage
                                    
                                Case Else
                                    
                                    OrderStatus = 0
                                    
                                    TmpMessage = "El servicio solicitado fue procesado, sin embargo, no pudo ser confirmado en estos momentos. " & _
                                    "El mismo ha sido a�adido a la cola de pendientes y ser� confirmado tan pronto como sea posible." & GetLines & _
                                    "Informaci�n adicional: " & TmpMessage
                                    
                            End Select
                            
ErrorConfirmacion:
                            
                            If Err.Number <> 0 Then
                                
                                Select Case ProductData("Code")
                                    
                                    Case "401"
                                        
                                        OrderStatus = 2
                                        
                                        TmpMessage = "No se recibi� respuesta por parte del proveedor de servicios. " & _
                                        "El servicio solicitado no fue procesado y ser� anulado en la operaci�n de cierre del turno. " & _
                                        "Informaci�n t�cnica adicional: " & Err.Description
                                        
                                    Case Else
                                        
                                        OrderStatus = 0
                                        
                                        TmpMessage = "El servicio solicitado fue procesado, sin embargo, no pudo ser confirmado en estos momentos. " & _
                                        "El mismo ha sido a�adido a la cola de pendientes y ser� confirmado tan pronto como sea posible." & GetLines & _
                                        "Informaci�n adicional: " & TmpMessage
                                        
                                End Select
                                
                            End If
                            
                            If Len(TmpMessage) > 0 Then Mensaje True, TmpMessage
                            
                        End If
                        
                    Else
                        
                        Select Case ProductData("Code")
                            
                            Case "401"
                                
                                OrderStatus = 2
                                
                                TmpMessage = "No se recibi� respuesta por parte del proveedor de servicios. " & _
                                "El servicio solicitado no fue procesado y ser� anulado en la operaci�n de cierre del turno. " & _
                                "Informaci�n t�cnica adicional: " & Err.Description
                                
                            Case Else
                                
                                OrderStatus = 0
                                
                                TmpMessage = "El servicio solicitado fue procesado, sin embargo, no pudo ser confirmado en estos momentos. " & _
                                "El mismo ha sido a�adido a la cola de pendientes y ser� confirmado tan pronto como sea posible." & GetLines & _
                                "Informaci�n adicional: " & TmpMessage
                                
                        End Select
                        
                        Mensaje True, TmpMessage
                        
                    End If
                    
                Else
                    
                    Select Case ProductData("Code")
                        
                        Case "401"
                            
                            OrderStatus = 2
                            
                            TmpMessage = "No se recibi� respuesta por parte del proveedor de servicios. " & _
                            "El servicio solicitado no fue procesado y ser� anulado en la operaci�n de cierre del turno. " & _
                            "Informaci�n t�cnica adicional: " & Err.Description
                            
                        Case Else
                            
                            OrderStatus = 0
                            
                            TmpMessage = "El servicio solicitado fue procesado, sin embargo, no pudo ser confirmado en estos momentos. " & _
                            "El mismo ha sido a�adido a la cola de pendientes y ser� confirmado tan pronto como sea posible." & GetLines & _
                            "Informaci�n adicional: " & TmpMessage
                            
                    End Select
                    
                    Mensaje True, TmpMessage
                    
                End If
                
                ' Fin proceso de confirmaci�n.
                
            ElseIf TmpResponseCode = "01" Or TmpResponseCode = "02" Then
                
RepararSesion:
                
                If (IntentosSesion <= MaxIntentosSesion) Then
                    
                    ServiceReady = False: TmpSecurityToken = vbNullString
                    
                    InitializeResources PrepaidServicesLocalCls, Company_User, Company_Password, Company_POS_ID
                    
                    IntentosSesion = IntentosSesion + 1
                    
                End If
                
                If ServiceReady Then ' Ya se renov�, reintentar recarga.
                    If (IntentosSesion <= MaxIntentosSesion) Then
                        GoTo Requery
                    Else
                        Mensaje True, "La sesi�n ha expirado y no se ha podido restablecer. El servicio no est� disponible, por favor intente mas tarde."
                    End If
                Else ' A�n no hay
                    Mensaje True, "La sesi�n ha expirado y no se ha podido restablecer. El servicio no est� disponible, por favor intente mas tarde."
                End If
                
            Else
                
                TmpMessage = "Su solicitud no pudo ser procesada. Informaci�n adicional:" & GetLines & _
                TmpMessage
                
ErrorSolicitud:
                
                If Err.Number <> 0 Then
                    TmpMessage = "No se recibi� respuesta por parte del proveedor de servicios. " & _
                    "El servicio solicitado no fue procesado y ser� anulado en la operaci�n de cierre del turno. " & _
                    "Informaci�n t�cnica adicional: " & Err.Description
                End If
                
                Mensaje True, TmpMessage
                
            End If
            
        Else
            Err.Raise -1, , "Fallo de comunicaci�n."
        End If
        
    Else
        If DebugMode Then Mensaje True, "Un error desconocido ha ocurrido al procesar su solicitud. Por favor intentelo de nuevo mas tarde."
        Err.Raise -1, , "Fallo de comunicaci�n."
    End If
    
    If OrderPlaced Then
        
        Set DLLResponseDataObject = New Collection
        
        With DLLResponseDataObject
            
            Collection_AddKey DLLResponseDataObject, "Compra de Servicios", "DLLAction"
            
            Collection_AddKey DLLResponseDataObject, ProductType, "ServiceType"
            Collection_AddKey DLLResponseDataObject, ProductData, "ProductData"
            
            Collection_AddKey DLLResponseDataObject, ImgProduct.Picture, "ProductImage"
            
            Collection_AddKey DLLResponseDataObject, OrderStatus, "OrderStatus"
            Collection_AddKey DLLResponseDataObject, OrderDate, "OrderDate"
            Collection_AddKey DLLResponseDataObject, OrderDateParam, "OrderDateSentString"
            Collection_AddKey DLLResponseDataObject, EncodeISOFullDate(OrderDate), "OrderDateISO"
            
            Collection_AddKey DLLResponseDataObject, TxtPhoneNumber.Enabled, "InputNumberRequired"
            Collection_AddKey DLLResponseDataObject, TxtPhoneNumber.Text, "InputNumber"
            
            Collection_AddKey DLLResponseDataObject, TotalFees, "Fee"
            Collection_AddKey DLLResponseDataObject, CDbl(TxtAmount.Text), "Amount"
            Collection_AddKey DLLResponseDataObject, TotalAmount, "Total"
            Collection_AddKey DLLResponseDataObject, TmpSerial, "SerialNumber"
            Collection_AddKey DLLResponseDataObject, Company_ID, "CompanyId"
            Collection_AddKey DLLResponseDataObject, Company_POS_ID, "POSId"
            Collection_AddKey DLLResponseDataObject, Company_UserID, "CompanyUserID"
            
            Collection_AddKey DLLResponseDataObject, TmpLogCode, "LogCode"
            Collection_AddKey DLLResponseDataObject, TmpPin, "Pin"
            Collection_AddKey DLLResponseDataObject, TmpCustomerID, "CustomerId"
            
            Collection_AddKey DLLResponseDataObject, TmpServiceSerial, "ServiceSerial"
            Collection_AddKey DLLResponseDataObject, TmpServicePin, "ServicePin"
            Collection_AddKey DLLResponseDataObject, TmpServiceLot, "ServiceLot"
            
            Collection_AddKey DLLResponseDataObject, (Not OrderConfirmed), "NeedsConfirmation"
            
            If OrderConfirmed Then
                Collection_AddKey DLLResponseDataObject, TmpAmount, "ConfirmationAmount"
                Collection_AddKey DLLResponseDataObject, TmpDaysOfProgramming, "DaysOfProgramming"
            Else
                Collection_AddKey DLLResponseDataObject, 0, "ConfirmationAmount"
                Collection_AddKey DLLResponseDataObject, 0, "DaysOfProgramming"
            End If
            
            ForcedExit
            
            Exit Sub
            
        End With
        
    End If
    
    WebDoc.Visible = False
    
End Sub

Private Sub lblDenomination_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

    For Each TmpItem In FrameDenomination
        TmpItem.BackColor = &H8000000D
    Next
    
    FrameDenomination(Index).BackColor = &H1BC957

    TxtAmount.Text = FormatNumber(Val(lblDenomination(Index).Caption), 2, vbTrue, vbFalse, vbTrue)
    
    SelectedDenominationIndex = Index
    
End Sub

Private Sub lblGoBack_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Not FrameSummary.Visible Then
        If FrameViewRates.Visible Or FrameViewToS.Visible Or FrameViewAccessNumbers.Visible Then
            OptionalViewResetIcons
        Else
            Unload Me
        End If
    Else
        OptionalViewResetIcons
        FrameSummary.Visible = False
        FrameMainView.Visible = True
    End If
End Sub

Private Sub lblProceed_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    Dim PayAmount As Double
    
    If Not IsNumeric(TxtAmount) Then
        Mensaje True, "Debe introducir un monto v�lido."
        Exit Sub
    End If
    
    PayAmount = CDbl(TxtAmount.Text)
    
    If MinAmount <> 0 Then
        If PayAmount < MinAmount Then
            Mensaje True, "El monto m�nimo permitido es " & FormatNumber(MinAmount, 2, vbTrue, vbFalse, vbTrue) & " Bs."
            Exit Sub
        End If
    End If
    
    If MaxAmount <> 0 Then
        If PayAmount > MaxAmount Then
            Mensaje True, "El monto m�ximo permitido es " & FormatNumber(MaxAmount, 2, vbTrue, vbFalse, vbTrue) & " Bs."
            Exit Sub
        End If
    End If
    
    If TxtPhoneNumber.Enabled Then
        If Trim(TxtPhoneNumber.Text) = vbNullString Then
            Mensaje True, "El " & ProductData("MainParameterName") & " es requerido. " & _
            "Por favor introduzca un " & ProductData("MainParameterName") & " v�lido."
            Exit Sub
        End If
    End If
    
    lblSelectedProductName.Caption = ProductData("CarrierName")
    
    Dim FeeType, Fee
    
    FeeType = Null 'ProductData("FeeType")
    Fee = Null 'ProductData("Fees")
    
    If Not IsNull(FeeType) Then
        
        ' Averiguar como hacer el calculo
        
        'Select Case FeeType
            'Case 0
            'Case 22
            'Case Else
        'End Select
        
        ' Por los momentos...
                        
        If IsNull(Fee) Then Fee = 0
                        
        If Fee <> 0 Then
            lblTotalFees.Caption = FormatNumber(Fee, 2, vbTrue, vbFalse, vbTrue) & " Bs."
        Else
            lblTotalFees.Caption = "N/A"
        End If
        
        lblTotalFees.Tag = CStr(CDbl(Fee))
        
        lblTotalAmount.Caption = FormatNumber(PayAmount + Fee, 2, vbTrue, vbFalse, vbTrue) & " Bs."
        lblTotalAmount.Tag = CStr(CDbl(PayAmount) + CDbl(Fee))
        
    Else
        lblTotalFees.Caption = "N/A"
        lblTotalFees.Tag = CStr(0)
        lblTotalAmount.Caption = FormatNumber(PayAmount, 2, vbTrue, vbFalse, vbTrue) & " Bs."
        lblTotalAmount.Tag = CStr(PayAmount)
    End If
    
    OptionalViewResetIcons
    FrameMainView.Visible = False
    
    Dim FinalTop As Long
    
    With FrameSummary
    
        FinalTop = .Top
        .Top = .Height * -1
        .Visible = True
        
        For i = .Top To FinalTop Step 20
            DoEvents
            .Top = i
        Next i
        
    End With
    
End Sub

Private Sub lblRates_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PicRates_MouseUp Button, Shift, x, y
End Sub

Private Sub lblTecladoKey_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
        
    If KBDFocusObj Is Nothing Then Set KBDFocusObj = TxtPhoneNumber
    
    KBDFocusObj.SetFocus
    
    If KBDFocusObj.Locked Then Exit Sub
    
    SendKeys lblTecladoKey(Index).Caption
        
End Sub

Private Sub lblToS_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PicToS_MouseUp Button, Shift, x, y
End Sub

Private Sub PicAccessNumbers_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    Dim FinalLeft As Long
    
    If PicAccessNumbers.Picture = PicInfo.Picture Then
        
        OptionalViewResetIcons
        
        Set PicAccessNumbers.Picture = PicBack.Picture
        
        FinalLeft = FrameViewAccessNumbers.Left
        FrameViewAccessNumbers.Left = Me.Width
        
        FrameViewAccessNumbers.Visible = True
        FrameViewAccessNumbers.ZOrder 0
        
        For i = Me.Width To FinalLeft Step -1
            DoEvents
            FrameViewAccessNumbers.Left = i
        Next i
        
    ElseIf PicAccessNumbers.Picture = PicBack.Picture Then
        OptionalViewResetIcons
    End If
    
End Sub

Private Sub PicRates_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    Dim FinalLeft As Long
    
    If PicRates.Picture = PicInfo.Picture Then
        
        OptionalViewResetIcons
        
        Set PicRates.Picture = PicBack.Picture
        
        FinalLeft = FrameViewRates.Left
        FrameViewRates.Left = Me.Width
        
        FrameViewRates.Visible = True
        FrameViewRates.ZOrder 0
        
        For i = Me.Width To FinalLeft Step -1
            DoEvents
            FrameViewRates.Left = i
        Next i
        
    ElseIf PicRates.Picture = PicBack.Picture Then
        OptionalViewResetIcons
    End If
    
End Sub

Private Sub PicTecladoKey_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    
    If KBDFocusObj Is Nothing Then Set KBDFocusObj = TxtPhoneNumber
    
    KBDFocusObj.SetFocus
    
    Select Case Index
        Case 12 ' Backspace
            If KBDFocusObj.Locked Then Exit Sub
            SendKeys Chr(vbKeyBack)
        Case 13 '
            SendKeys Chr(vbKeyReturn)
        Case 14
            If KBDFocusObj.Locked Then Exit Sub
            KBDFocusObj.Text = vbNullString
    End Select
    
End Sub

Private Sub OptionalViewResetIcons()
    Set PicRates.Picture = PicInfo.Picture
    If FrameViewRates.Visible Then FrameViewRates.Visible = False
    Set PicToS.Picture = PicInfo.Picture
    If FrameViewToS.Visible Then FrameViewToS.Visible = False
    Set PicAccessNumbers.Picture = PicInfo.Picture
    If FrameViewAccessNumbers.Visible Then FrameViewAccessNumbers.Visible = False
End Sub

Private Sub PicToS_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    Dim FinalLeft As Long
    
    If PicToS.Picture = PicInfo.Picture Then
        
        OptionalViewResetIcons
        
        Set PicToS.Picture = PicBack.Picture
        
        FinalLeft = FrameViewToS.Left
        FrameViewToS.Left = Me.Width
        
        FrameViewToS.Visible = True
        FrameViewToS.ZOrder 0
        
        For i = Me.Width To FinalLeft Step -30
            DoEvents
            FrameViewToS.Left = i
        Next i
        
    ElseIf PicToS.Picture = PicBack.Picture Then
        OptionalViewResetIcons
    End If
    
End Sub

Private Sub TxtAmount_Click()
    Set KBDFocusObj = TxtAmount
End Sub

Private Sub TxtAmount_GotFocus()
    Set KBDFocusObj = TxtAmount
End Sub

Private Sub TxtAmount_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If PuedeObtenerFoco(TxtPhoneNumber) Then TxtPhoneNumber.SetFocus
    End If
End Sub

Private Sub TxtPhoneNumber_Click()
    Set KBDFocusObj = TxtPhoneNumber
End Sub

Private Sub TxtPhoneNumber_GotFocus()
    Set KBDFocusObj = TxtPhoneNumber
End Sub

Private Sub TxtPhoneNumber_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If PuedeObtenerFoco(TxtAmount) Then TxtAmount.SetFocus
    End If
End Sub
