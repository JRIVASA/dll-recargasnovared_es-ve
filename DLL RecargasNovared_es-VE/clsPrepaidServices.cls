VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPrepaidServices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Function VP(pC)
    Select Case pC
        Case StlBUSINESS, StlFOOD, StlPOS
            VP = x = y
    End Select
End Function

Public Property Get InputParams() As Dictionary
    Set InputParams = Variables.InputParams
End Property

Public Property Let InputParams(Params As Dictionary)
    Set Variables.InputParams = Params
End Property

Public Function ReleasePhoneServiceOrder(pSerialNumber As String, pWebServiceVersionNumber As String, pPrepaidRootDir As String, _
pID As Long, _
pImageDir As String, pProductDataDir As String, pTopSoldProductsDir As String, _
pTicketNumber As String, _
Optional ByVal pInitialize As Boolean = True) As Object
    
    Set ReleasePhoneServiceOrder = New Dictionary
    
    If Not VP(pID) Then Exit Function
    
    If pInitialize Then
        
        InitializeResources Me, pSerialNumber, pWebServiceVersionNumber, pPrepaidRootDir
        
        PhoneServices_ProductImageDir = pImageDir
        PhoneServices_ProductDataDir = pProductDataDir
        PhoneServices_TopSoldProductsDir = pTopSoldProductsDir
        
    End If
    
    If Not CanReleaseOrders Then
        ReleasePhoneServiceOrder.Add "CanReleaseOrders", CanReleaseOrders
        ReleasePhoneServiceOrder.Add "OperationSuccess", True
        Exit Function
    End If
    
    ' LLamar a WebService y Terminar de Ejecutar la Orden....
    
    ' --- Get Top Sold Products ---
    
    DoEvents
    
    ReFocus
    
    On Error Resume Next
    
    Dim ReleaseQuery As MSXML2.ServerXMLHTTP
    
    Set ReleaseQuery = New MSXML2.ServerXMLHTTP
    
    sURL = "http://bsapi.pinserve.com/api/orders/executeorder"
    
    ReleaseQuery.open "POST", sURL, False
    ReleaseQuery.setRequestHeader "Content-Type", "application/json"
    ReleaseQuery.setRequestHeader "Cache-Control", "no-cache"
    ReleaseQuery.setRequestHeader "Postman -Token", "7e5e1b91-a112-2951-131e-7642e19e466c"
    
    Dim Params As String, Response As String, TicketData, Success As Boolean
    
    Params = "{"
    
    Params = Params & GetLines & vbTab & """OrderNumber"":" & pTicketNumber & ","
    Params = Params & GetLines & vbTab & """SerialNumber"":""" & SerialNumber & ""","
    Params = Params & GetLines & vbTab & """MerchantId"":" & MerchantID & ","
    Params = Params & GetLines & vbTab & """TerminalId"":" & TerminalID & ""
    
    Params = Params & GetLines & "}"
    
    ReleaseQuery.send Params
    
    DoEvents
    
    ReFocus
    
    Response = ReleaseQuery.responseText
    Set TicketData = JSON.parse(Response)
    
    Success = Not (TicketData Is Nothing)
    
    If Success Then
        Success = (Trim(TicketData("Status")) = "200")
        TicketNumber = Trim(TicketData("Ticket"))
        'Debug.Print Trim(TicketData("ErrorMessage"))
        
        Set ReleasePhoneServiceOrder = TicketData
    End If
    
    ReleasePhoneServiceOrder.Add "CanReleaseOrders", CanReleaseOrders
    ReleasePhoneServiceOrder.Add "OperationSuccess", Success
    
End Function

Public Function ShowPhoneServices(ByVal pUser As String, ByVal pPassword As String, _
ByVal pPOSId As String, ByVal pID As Long, _
Optional ByVal pInitialize As Boolean = True) As Object
    
    If Not VP(pID) Then Exit Function
    
    If pInitialize Then
        StellarProductID = pID
        InitializeResources Me, pUser, pPassword, pPOSId
    End If
    
    FrmPhoneServices.Show vbModal
    
    Set FrmPhoneServices = Nothing
    
    Set ShowPhoneServices = DLLResponseDataObject
    
End Function

Public Sub ShowFrmOrder()
    FrmPhoneProducts_Order.Show vbModal
End Sub

Public Sub UpdatePhoneProducts(pSerialNumber As String, pWebServiceVersionNumber As String, pPrepaidRootDir As String, _
pID As Long, _
pImageDir As String, pProductDataDir As String, pTopSoldProductsDir As String, _
Optional ByVal pInitialize As Boolean = True)
    
    If Not VP(pID) Then Exit Sub
    
    If pInitialize Then
        
        InitializeResources Me, pSerialNumber, pWebServiceVersionNumber, pPrepaidRootDir
        
        PhoneServices_ProductImageDir = pImageDir
        PhoneServices_ProductDataDir = pProductDataDir
        PhoneServices_TopSoldProductsDir = pTopSoldProductsDir
        
    End If
    
    Dim TmpFilePath As String
    
    ' --- Get Products for each Type --
    
    For i = PhoneServices_ProductTypeLBound To PhoneServices_ProductTypeUBound
        
        On Error GoTo ErrProductType
        
        Select Case i
            Case TypeLongDistance, TypePinless, TypeDomesticTopup, _
            TypeInternationalTopup, TypeSunpass
                
                Dim ProductType As PhoneServices_ProductType
                Dim ProductDataReady As Boolean
                Dim ProductData As Object
                Dim ProductResponse As String
                Dim ProductCode As String
                
                Dim ProductsQuery As MSXML2.ServerXMLHTTP, ProductImageQuery As MSXML2.ServerXMLHTTP
                
                ProductType = i
                
                sURL = "http://prepaidupdate.blackstoneonline.com/api/products?merchantId=" & MerchantID & "&productType=" & ProductType
                
                Set ProductsQuery = New MSXML2.ServerXMLHTTP
                ProductsQuery.open "GET", sURL, False
                ProductsQuery.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
                ProductsQuery.send
                
                DoEvents
                
                ReFocus
                
                If CancelLongRunningOperation Then Exit Sub
                
                ProductResponse = ProductsQuery.responseText
                Set ProductData = JSON.parse(ProductResponse)
                
                ProductDataReady = Not (ProductData Is Nothing)
                
                TmpFilePath = FindPath("ProductData.json", _
                NO_SEARCH_MUST_FIND_EXACT_MATCH, pProductDataDir & "/" & ProductType)
                
                If Not PathExists(TmpFilePath) Then CreateFullDirectoryPath TmpFilePath
                
                Call SaveTextFile(ProductResponse, TmpFilePath)
                                
                If ProductDataReady Then
                    
                    For Each Product In ProductData
                                                                    
                        On Error GoTo ErrProduct
                        
                        Dim RawImageDate As String, ImageDate As Date
                        Dim ImageContentLength As Double, NeedsUpdate As Boolean
                        Dim TmpFileDate As Date, TmpFileLength As Double
                                                                    
                        ProductCode = Product("Code")
                                            
                        sURL = "http://prepaidupdate.blackstoneonline.com/api/images/" & ProductCode
                        
                        Set ProductImageQuery = New MSXML2.ServerXMLHTTP
                        
                        ProductImageQuery.open "GET", sURL, False
                        ProductImageQuery.send
                        
                        DoEvents
                        
                        ReFocus
                        
                        If CancelLongRunningOperation Then Exit Sub
                        
                        TmpFilePath = FindPath("ProductImage.png", _
                        NO_SEARCH_MUST_FIND_EXACT_MATCH, pImageDir & "/" & ProductType & "/" & ProductCode)
                        
                        RawImageDate = ProductImageQuery.getResponseHeader("X-Image-Date")
                        ImageDate = DecodeISOFullDate(RawImageDate)
                        
                        If PathExists(TmpFilePath) Then
                            ImageContentLength = Val(ProductImageQuery.getResponseHeader("Content-Length"))
                            
                            TmpFileDate = FileDateTime(TmpFilePath)
                            TmpFileLength = FileLen(TmpFilePath)
                            
                            NeedsUpdate = (TmpFileDate > ImageDate) Or _
                            Abs(TmpFileLength - ImageContentLength) > Fix(ImageContentLength * (1 / 100))
                        Else
                            NeedsUpdate = True
                        End If
                        
                        If NeedsUpdate Then
                            
                            DoEvents
                            
                            ReFocus
                            
                            If CancelLongRunningOperation Then Exit Sub
                                                        
                            If Not PathExists(TmpFilePath) Then CreateFullDirectoryPath TmpFilePath
                            
                            If Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, GetDirParent(TmpFilePath))) Then
                                MkDir PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, GetDirParent(TmpFilePath)))
                            End If
                            
                            Dim TmpFileStream As ADODB.Stream
                            
                            Set TmpFileStream = New ADODB.Stream
                            
                            TmpFileStream.Type = adTypeBinary
                            TmpFileStream.open
                            TmpFileStream.Write (ProductImageQuery.responseBody)
                            
                            TmpFileStream.SaveToFile TmpFilePath, adSaveCreateOverWrite
                            TmpFileStream.Close
                            
                            SetFileDateTime TmpFilePath, ImageDate
                            
                        End If
                        
                        GoTo ContinueProducts
ErrProduct:
    Err.Clear
    Resume ContinueProducts
ContinueProducts:
                    Next
                    
                End If
                                                
            Case Else
                ' Ignore
        End Select
        
        GoTo ContinueProductTypes
        
ErrProductType:
    Resume ContinueProductTypes
ContinueProductTypes:
    Next i
    
    ' --- Get Top Sold Products ---
    
    DoEvents
    
    ReFocus
    
    If CancelLongRunningOperation Then Exit Sub
    
    On Error Resume Next
    
    Dim TopSoldQuery As New MSXML2.ServerXMLHTTP
    
    sURL = "http://prepaidupdate.blackstoneonline.com/api/merchants/IndexByMerchant/?merchantId=" & MerchantID
    
    TopSoldQuery.open "GET", sURL, False
    TopSoldQuery.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    TopSoldQuery.send
    
    DoEvents
    
    ReFocus
    
    If CancelLongRunningOperation Then Exit Sub
    
    ProductResponse = TopSoldQuery.responseText
    Set ProductData = JSON.parse(ProductResponse)
    
    ProductDataReady = Not (ProductData Is Nothing)
    
    TmpFilePath = FindPath("TopSoldItems.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, pTopSoldProductsDir)
    
    If Not PathExists(TmpFilePath) Then CreateFullDirectoryPath TmpFilePath
    
    Call SaveTextFile(ProductResponse, TmpFilePath)

    ' --- Determine Country List which offer International Service.
    
    DoEvents
    
    ReFocus
    
    If CancelLongRunningOperation Then Exit Sub
    
    TmpFilePath = FindPath("ProductData.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, pProductDataDir & "/" & TypeInternationalTopup)
    
    If PathExists(TmpFilePath) Then
            
        ProductResponse = LoadTextFile(TmpFilePath)
        
        Set ProductData = JSON.parse(ProductResponse)
        
        ProductDataReady = Not (ProductData Is Nothing)
        
        If ProductDataReady Then
            
            Dim CountryCollection As New Dictionary, CountryData As Variant, CountryJSON As String
            
            For Each Product In ProductData
                                                
                DoEvents
                
                If CancelLongRunningOperation Then Exit Sub
                                                
                If Not CountryCollection.Exists(CStr(Product("CountryCode"))) Then
                    
                    Set CountryData = New Collection
                    
                    CountryData.Add Product("Country"), "Name"
                    CountryData.Add Product("CountryCode"), "Code"
                    
                    CountryCollection.Add CountryData("Code"), CountryData
                    
                End If
                                
            Next
            
            If CountryCollection.Count > 0 Then
                    
                CountryJSON = "["
                    
                For Each CountryData In CountryCollection.Items()
               
                    DoEvents
                    
                    If CancelLongRunningOperation Then Exit Sub
               
                    CountryJSON = CountryJSON & GetLines
                    CountryJSON = CountryJSON & "{""Name"": """ & CountryData("Name") & """, ""Code"": """ & CountryData("Code") & """},"
               
                Next
                            
                CountryJSON = Left(CountryJSON, Len(CountryJSON) - 1) ' Remove Extra ","
                            
                CountryJSON = CountryJSON & GetLines & "]"
                
                TmpFilePath = FindPath("InternationalCountryData.json", _
                NO_SEARCH_MUST_FIND_EXACT_MATCH, pProductDataDir)
                
                If Not PathExists(TmpFilePath) Then CreateFullDirectoryPath TmpFilePath
                
                SaveTextFile CountryJSON, TmpFilePath
                
            End If
            
        End If
        
    End If
    
    ' Save current Last Execution Date.
    
    DoEvents
    
    ReFocus
    
    If CancelLongRunningOperation Then Exit Sub
    
    TmpFilePath = FindPath("LastAutoUpdate.txt", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir)
    
    If Not PathExists(TmpFilePath) Then CreateFullDirectoryPath TmpFilePath
    
    Call SaveTextFile(EncodeISOFullDate(Now), TmpFilePath)
    
End Sub

Private Sub Class_Terminate()
    TmpSecurityToken = vbNullString
    ServiceURL = vbNullString
    ServiceAction = vbNullString
End Sub
