Attribute VB_Name = "Functions"
Public Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Function PathExists(ByVal pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
     If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStr(1, pPath, "\")
    
    If Pos <> 0 Then
        GetDirectoryRoot = Left(pPath, Pos - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStrRev(pPath, "\")
    
    If Pos <> 0 Then
        GetDirParent = Left(pPath, Pos - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function GetLines(Optional HowMany As Long = 1)
        
    Dim HowManyLines As Integer
    
    HowManyLines = ValidarNumeroIntervalo(HowMany, , 1)
        
    Dim i
        
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function EncodeISOFullDate(pDate As Date) As String
    
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("yyyy", pDate), "0000")
    EncodeISOFullDate = EncodeISOFullDate & "-"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("m", pDate), "00")
    EncodeISOFullDate = EncodeISOFullDate & "-"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("d", pDate), "00")
    
    EncodeISOFullDate = EncodeISOFullDate & "T"
    
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("h", pDate), "00")
    EncodeISOFullDate = EncodeISOFullDate & ":"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("n", pDate), "00")
    EncodeISOFullDate = EncodeISOFullDate & ":"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("s", pDate), "00")

End Function

Public Function DecodeISOFullDate(ByVal pISOString As String) As Date
            
    DecodeISOFullDate = CDate(CStr( _
    DateSerial(CInt(Mid(pISOString, 1, 4)), CInt(Mid(pISOString, 6, 2)), CInt(Mid(pISOString, 9, 2))) & " " & _
    TimeSerial(CInt(Mid(pISOString, 12, 2)), CInt(Mid(pISOString, 15, 2)), CInt(Mid(pISOString, 18, 2)))))
    
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    
    On Error Resume Next
    
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
    
End Function

Public Function LoadFile(StrFileName As _
String) As String

    On Error GoTo Err

        Dim sFileText As String
        Dim iFileNo As Integer
        iFileNo = FreeFile
        'open the file for reading
        Open StrFileName For Input As #iFileNo
         Dim Tmp
        'read the file until we reach the end
        i = 1
        Do While Not EOF(iFileNo)
          Tmp = Input(1, #iFileNo)
          LoadFile = LoadFile & CStr(Tmp)
          i = i + 1
        Loop

        'close the file (if you dont do this, you wont be able to open it again!)
        Close #iFileNo
        
    Exit Function
    
Err:
    
    Debug.Print Err.Description

End Function

Public Function LoadTextFile(ByVal pFilePath As String) As String
    
    On Error GoTo Error
    
    FrmLoadTextFile.RDoc.LoadFile pFilePath, rtfText
    LoadTextFile = FrmLoadTextFile.RDoc.Text
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

Public Function SaveTextFile(ByVal pContents As String, ByVal pFilePath As String, _
Optional pUseUnicode As Boolean = False) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(pFilePath, True, pUseUnicode)
    TStream.Write (pContents)
    TStream.Close
    
    SaveTextFile = True

    Exit Function
    
ErrFile:
    
    Debug.Print Err.Description
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function Collection_AddKey(pCollection As Collection, pValor, pKey As String) As Boolean
    
    On Error GoTo Err
    
    pCollection.Add pValor, pKey
    
    Collection_AddKey = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_AddKey = False
    
End Function

'Public Function Collection_IgualA(pCollection As Collection, pValor) As Boolean
'
'    On Error GoTo Err
'
'    Dim i As Long
'
'    For i = 1 To pCollection.Count
'        If pCollection.Item(i) = pValor Then Collection_IgualA = True: Exit Function
'    Next i
'
'    Exit Function
'
'Err:
'
'    Debug.Print Err.Description
'
'    Collection_IgualA = False
'
'End Function

Public Function Collection_EncontrarValor(pCollection As Collection, pValor, Optional pIndiceStart As Long = 1) As Long
    
    On Error GoTo Err
    
    Dim i As Long
    
    For i = pIndiceStart To pCollection.Count
        If pCollection.Item(i) = pValor Then Collection_EncontrarValor = i: Exit Function
    Next i
    
    Collection_EncontrarValor = -1
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_EncontrarValor = -1
    
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    'Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function Collection_ExisteIndex(pCollection As Collection, pIndex As Long, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pIndex)
    Else
        Set tmpValorObj = pCollection.Item(pIndex)
    End If
     
    Collection_ExisteIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteIndex = False
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveKey = False
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveIndex = False
    
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
    End If
End Sub

Public Sub TecladoWindows(Optional pControl As Object)
    
    On Error Resume Next
    
    ' Abrir el Teclado.
    
    Dim Ruta As String
    
    If WindowsArchitecture = [32Bits] Then
                    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            
    ElseIf WindowsArchitecture = [64Bits] Then
    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramW6432") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then
            res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
        Else
            
            Ruta = FindPath("TabTip32.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles(X86)") & "\Common Files\Microsoft Shared\Ink")
            
            If PathExists(Ruta) Then
                res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            End If
            
        End If
        
    End If
    
    If PuedeObtenerFoco(pControl) Then pControl.SetFocus
    
End Sub

Public Sub InitializeResources(pClsLocal As clsPrepaidServices, _
ByVal pUser As String, ByVal pPassword As String, ByVal pPOSId As String)
    
    TmpVal = GetEnvironmentVariable("ProgramW6432")
    
    Select Case Len(Trim(TmpVal))
        Case 0 ' Default / No Especificado.
            WindowsArchitecture = [32Bits]
        Case Else
            WindowsArchitecture = [64Bits]
    End Select
    
    Set PrepaidServicesLocalCls = pClsLocal
    
    'Set TmpAccessParams = New Dictionary
    
    'TmpAccessParams("ProductCode") = 881: TmpAccessParams("ProductName") = "StellarPrepaidServices": TmpAccessParams("PK") = Chr(78) & Chr(111) & _
    Chr(118) & Chr(97) & Chr(82) & Chr(101) & Chr(100) & Chr(95) & Chr(55) & Chr(75) & Chr(51) & Chr(46) & Chr(53) & Chr(56) & Chr(78) & _
    Chr(68) & Chr(73) & Chr(57) & Chr(49) & Chr(46) & Chr(55) & Chr(95) & Chr(90) & Chr(49) & Chr(121) & Chr(77) & Chr(100) & Chr(88)
    
    'If .Usuario <> vbNullString And .Clave <> vbNullString Then
        
        'Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        'If Not mClsTmp Is Nothing Then
            'Company_User = mClsTmp.Decode(TmpAccessParams("ProductCode"), TmpAccessParams("ProductName"), TmpAccessParams("PK"), pUser)
            'Company_Password = mClsTmp.Decode(TmpAccessParams("ProductCode"), TmpAccessParams("ProductName"), TmpAccessParams("PK"), pPassword)
            
            Company_User = pUser: Company_Password = pPassword
            
        'End If
        
    'End If
    
    If Not InputParams.Exists("Moneda_Simbolo") Then
        InputParams("Moneda_Simbolo") = "Bs."
    End If
    
    Company_POS_ID = pPOSId
    
    DebugMode = InputParams("DebugMode")
    
    SecurityProtocol = InputParams("SecurityProtocol")
    
    If SecurityProtocol Then
        ServiceURL = "https://qasco1.novared.net.ve:46026/cryptows/services/CommercialCompanyScolopendraWebService"
    Else
        ServiceURL = "https://qasco1.novared.net.ve:46025/cryptows/services/CommercialCompanyScolopendraWebService"
    End If
    
    If Trim(InputParams("URLServicio")) <> vbNullString Then
        ServiceURL = InputParams("URLServicio")
    End If
    
    ServiceAction = ServiceURL & "/getSecurityToken"
    
    If DebugMode Then Mensaje True, "URL:" & vbNewLine & ServiceURL
    If DebugMode Then Mensaje True, "Action:" & vbNewLine & ServiceAction
    
    Dim TmpFilePath As String, TmpFileContents As String, TmpInfo As Object
    
    Dim MaxTries As Integer, CurrentTry As Integer
    
    MaxTries = 3
    CurrentTry = 1
    
GetInfo:
    
    If TmpSecurityToken = vbNullString And CurrentTry <= MaxTries Then
        
        On Error GoTo ErrReadDetails
        
        TmpSOAPUrl = ServiceURL
        TmpSOAPAction = ServiceURL & "/getSecurityToken"
    
        TmpRequest = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
        "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
        "<soap:Body>" & _
        "<getSecurityToken xmlns=""http://ws.novared.com.ve/"">" & _
        "<userName>" & Company_User & "</userName>" & _
        "<password>" & Company_Password & "</password>" & _
        "</getSecurityToken>" & _
        "</soap:Body>" & _
        "</soap:Envelope>"
        
        If DebugMode Then Mensaje True, "Request:" & vbNewLine & Replace(Replace(TmpRequest, _
        "<userName>" & Company_User & "</userName>", "<userName>...</userName>"), _
        "<password>" & Company_Password & "</password>", "<password>...</password>")
        
        Dim RequestDocument         As DOMDocument60
        Dim ResponseDocument        As DOMDocument60
        Dim MerchantDetailsQuery    As MSXML2.ServerXMLHTTP
        Dim TmpResponse             As String
        
        If DebugMode Then
            Mensaje True, "Instanciando Objetos DOMDocument60, MSXML2.ServerXMLHTTP"
        End If
        
        Set RequestDocument = New DOMDocument60
        Set MerchantDetailsQuery = New MSXML2.ServerXMLHTTP
        
        If DebugMode Then
            Mensaje True, "Procediendo a cargar Request en el RequestDocument"
        End If
        
        RequestDocument.async = False
        RequestDocument.loadXML TmpRequest
        
        MerchantDetailsQuery.open "POST", TmpSOAPUrl, False
        
        MerchantDetailsQuery.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        MerchantDetailsQuery.setRequestHeader "SOAPAction", TmpSOAPAction
        
        DoEvents
        
        If DebugMode Then
            Mensaje True, "Enviando RequestDocument: " & vbNewLine & vbNewLine & _
            RequestDocument.xml
        End If
        
        MerchantDetailsQuery.send RequestDocument.xml
        
        DoEvents
        
        TmpResponse = MerchantDetailsQuery.responseText
        
        Set ResponseDocument = New MSXML2.DOMDocument60
        
        If DebugMode Then
            Mensaje True, "Raw Response: " & vbNewLine & TmpResponse & vbNewLine & vbNewLine & _
            "Intentando cargar ResponseDocument"
        End If
        
        If ResponseDocument.loadXML(TmpResponse) Then
            
            If DebugMode Then
                Mensaje True, "ResponseDocument cargado. Procediendo a habilitar Namespaces y obtener ns1:getSecurityTokenReturn"
            End If
            
            ResponseDocument.setProperty "SelectionLanguage", "XPath"
            
            ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve/'"
            
            Set getSecurityTokenResponse = ResponseDocument.selectSingleNode("/soapenv:Envelope/soapenv:Body/ns1:getSecurityTokenResponse")
            
            ResponseDocument.setProperty "SelectionNamespaces", "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://ws.novared.com.ve'"
            
            Set getSecurityTokenReturn = getSecurityTokenResponse.selectSingleNode("ns1:getSecurityTokenReturn")
            
            Set Result = New DOMDocument60
            
            Result.setProperty "SelectionLanguage", "XPath"
            
            If DebugMode Then Mensaje True, "RespuestaGetSecurityToken:" & vbNewLine & vbNewLine & getSecurityTokenReturn.Text
            
            If Result.loadXML(getSecurityTokenReturn.Text) Then
                
                If DebugMode Then Mensaje True, "getSecurityTokenXML:" & vbNewLine & vbNewLine & Result.xml
                
                If Result.selectSingleNode("result/response_code").Text = "00" Then
                    
                    TmpResponseCode = Result.selectSingleNode("result/response_code").Text
                    Company_ID = Result.selectSingleNode("result/company").Text
                    Company_UserID = Result.selectSingleNode("result/user").Text
                    TmpSecurityToken = Result.selectSingleNode("result/security_token").Text
                    Set TmpCarrierList = Result.selectSingleNode("result/company_list")
                    
                    ServiceReady = True
                    
                Else
                    
                    CurrentTry = CurrentTry + 1
                    
                    If CurrentTry > MaxTries Then
                        Mensaje True, "Respuesta enviada por el Servicio de Recargas: " & vbNewLine & _
                        Result.selectSingleNode("result/user_message").Text
                    End If
                    
                End If
                
            Else
                
                Set Error = Result.parseError
                
                With Error
                    
                    Mensaje True, "No se pudo hacer el Parse de la respuesta XML. " & _
                    vbNewLine & vbNewLine & _
                    "Detalles del Error: " & vbNewLine & _
                    "Codigo:" & .errorCode & vbNewLine & _
                    "Motivo:" & .reason & vbNewLine & _
                    "Linea:" & .Line & vbNewLine & _
                    "Src: " & vbNewLine & vbNewLine & .Source & vbNewLine
                    
                End With
                
                CurrentTry = CurrentTry + 1
                
            End If
            
        Else
            
            Set Error = Result.parseError
            
            With Error
                
                Mensaje True, "No se pudo hacer el Parse de la respuesta XML. " & _
                vbNewLine & vbNewLine & _
                "Detalles del Error: " & vbNewLine & _
                "Codigo:" & .errorCode & vbNewLine & _
                "Motivo:" & .reason & vbNewLine & _
                "Linea:" & .Line & vbNewLine & _
                "Src: " & vbNewLine & vbNewLine & .Source & vbNewLine
                
            End With
            
            CurrentTry = CurrentTry + 1
            
        End If
        
        GoTo GetInfo
        
ErrReadDetails:
        
        mErrorDesc = Err.Description
        mErrorNumber = Err.Number
        
        If DebugMode Then Mensaje True, mErrorDesc & vbNewLine & mErrorNumber
        
        CurrentTry = CurrentTry + 1
        
        Resume GetInfo
        
    End If
    
    Set DLLResponseDataObject = Nothing
    
End Sub

Public Sub ReFocus()
    
    On Error Resume Next
    
    If Not Screen.ActiveForm Is Nothing Then
        If PuedeObtenerFoco(Screen.ActiveForm) Then
            Screen.ActiveForm.SetFocus
        End If
    End If
    
End Sub

Public Sub ForcedExit()
    On Error GoTo Error
    While (Not Screen.ActiveForm Is Nothing)
        Unload Screen.ActiveForm
    Wend
Error:
End Sub

Public Function Mensaje(ByVal Activo As Boolean, ByVal Texto As String, Optional pVbmodal = True, _
Optional txtBoton1 As Variant, Optional txtBoton2 As Variant) As Boolean
    
    On Error GoTo Falla_Local
    
    If Not IsMissing(txtBoton1) Then txtMensaje1 = CStr(txtBoton1) Else txtMensaje1 = ""
    If Not IsMissing(txtBoton2) Then txtMensaje2 = CStr(txtBoton2) Else txtMensaje2 = ""
    
    If Not frm_Mensajeria.Visible Then
        
        frm_Mensajeria.Uno = Activo
        frm_Mensajeria.Mensaje.Text = frm_Mensajeria.Mensaje.Text & IIf(frm_Mensajeria.Mensaje.Text <> "", " ", "") & Texto
        
        If pVbmodal Then
            If Not frm_Mensajeria.Visible Then
                Retorno = False
                
                frm_Mensajeria.Show vbModal
                
                Set frm_Mensajeria = Nothing
            End If
        Else
            Retorno = False
            
            frm_Mensajeria.Show
            
            frm_Mensajeria.Aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
        End If
    End If
    
    Mensaje = Retorno
    
Falla_Local:

End Function

Public Function NOVARED_EsProductoEstacionamiento(ByVal pCodigoInternoStellar As String) As Boolean
    
    On Error GoTo Error
    
    If Not InputParams.Exists("ProductosEstacionamiento") Then Exit Function
    
    NOVARED_EsProductoEstacionamiento = _
    InputParams("ProductosEstacionamiento").Exists(pCodigoInternoStellar)
    
    Exit Function
    
Error:
    
End Function

Public Sub SafeLoadPicture(ByRef pPicObj, ByVal pFilePath)
    On Error Resume Next
    Set pPicObj.Picture = LoadPicture() ' Clean / Reset
    Set pPicObj.Picture = LoadPicture(pFilePath)
End Sub
