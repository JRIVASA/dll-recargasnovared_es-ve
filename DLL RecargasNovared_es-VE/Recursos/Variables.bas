Attribute VB_Name = "Variables"
Global PrepaidRootDir As String
Global PhoneServices_ProductImageDir As String
Global PhoneServices_ProductDataDir As String
Global PhoneServices_TopSoldProductsDir As String

Global StellarProductID As Long
Global StellarADMConnection As Object
Global StellarPOSConnection As Object

Global WindowsArchitecture As OperatingSystemArchitecture

Global PrepaidServicesLocalCls As clsPrepaidServices
Global ServiceReady As Boolean

Global ServiceVersionNumber As String

Global Company_ID       As String
Global Company_UserID   As String
Global Company_POS_ID   As String

Global Company_User     As String
Global Company_Password As String

Global TmpSecurityToken As String
Global TmpResponseCode  As String
Global TmpCarrierList   As Object

Global DLLResponseDataObject As Object

Global Retorno As Boolean

Global CancelLongRunningOperation As Boolean

Global InputParams      As Dictionary

Global SecurityProtocol         As Boolean
Global ServiceURL               As String
Global ServiceAction            As String

Public DebugMode                As Boolean
