-- Nuevo. One Time Script Events. Scripts que se ejecutan Una sola vez en la BD
-- y literalmente quedan en un estatus de 'Procesado'. Ideal para cambios �nicos
-- de datos, entre otros prop�sitos. Los scripts pueden poseer actualizaciones
-- basados en la fecha de emisi�n, la cual se introduce manualmente, al igual
-- que el resto de los datos del script. Si un script es actualizado con una 
-- fecha de emisi�n mayor a la anterior, el script tendr� la posibilidad de 
-- ser ejecutado y procesado nuevamente una sola vez, y asi sucesivamente.

-- EL 0 EN VEZ DE LA 'O' EN EL NOMBRE DE LA TABLA ES PARA QUE SALGA DE PRIMERA EN LA LISTA DE TABLAS
-- CADA BASE DE DATOS MANEJARA UNA TABLA CON EL NOMBRE ANTERIOR Y POSEE CAMBIOS INDEPENDIENTES.
-- O EN EL CASO DE QUE UN SCRIPT AFECTARA SIMULTANEAMENTE VARIAS BASES DE DATOS, LOS DATOS DE EJECUCION
-- SOLO DEBERIAN GRABARSE EN UNA, PREFERIBLEMENTE LA MAS RELEVANTE O LA QUE POSEA MAS CAMBIOS.

-- EL SIGUIENTE SCRIPT INCLUYE VARIABLES DECLARADAS PARA SU PROCESAMIENTO. EVIDENTEMENTE AL ESTABLECER
-- NUEVAS INSTANCIAS DE ESTE TIPO DE SCRIPTS DE AHORA EN ADELANTE EN ESTE SCRIPT ACUMULATIVO, SE DEBERAN
-- REMOVER O COMENTAR LAS LINEAS QUE CONTIENEN DECLARE, PUESTO QUE LAS MISMAS VARIABLES YA FUERON DECLARADAS 
-- ANTERIORMENTE. UNICAMENTE SE DEBER� USAR LA INSTRUCCION SET DIRECTAMENTE.

DECLARE @TmpVarScriptID NVARCHAR(256)
DECLARE @TmpVarIssueDate DATETIME
DECLARE @TmpVarApplyDate DATETIME
DECLARE @TmpVarScriptComments NVARCHAR(MAX)

DECLARE @TmpVarUpdateRow INT
DECLARE @TmpVarQueryDate DATETIME

-- SE DEBE MANTENER PRECAUCI�N DEBIDO A QUE DE MANERA GENERAL, UTILIZAMOS EN ALGUNOS SCRIPTS DE ESTE TIPO
-- LA INSTRUCCION SET DATEFORMAT XXX PARA CONTROLAR EL MANEJO DE DATE/DATETIME. SEGUIDO DEL FIN DEL SCRIPT,
-- SE DEBE REESTABLECER EL PAR�METRO DATEFORMAT A SU VALOR ANTERIOR, O ESTAR CONSCIENTE DEL VALOR UTILIZADO
-- POR EL SCRIPT PARA FUTURAS INSTRUCCIONES. EN LINEAS GENERALES, SE DEBE TENER CUIDADO CUANDO SE GENERAN
-- SCRIPTS QUE MANEJAN FECHAS DE UN CIERTO MODO, SI RESULTASE DISTINTO AL VALOR DE DATEFORMAT UTILIZADO
-- POR EL SCRIPT, COMO PODR�A SER EL CASO DE SCRIPTS AUTO-GENERADOS DESDE EL MANAGEMENT STUDIO.

USE [VAD10]

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '0NE_TIME_SCRIPT_EVENTS')
BEGIN
  CREATE TABLE [dbo].[0NE_TIME_SCRIPT_EVENTS](
    [ScriptID] [nvarchar](256) NOT NULL,
    [IssueDate] datetime NOT NULL,
    [AppliedOn] datetime NOT NULL,
    [Comments] [nvarchar](max) NOT NULL,
   CONSTRAINT [PK_0NE_TIME_SCRIPT_EVENTS] PRIMARY KEY CLUSTERED
  (
    [ScriptID] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '0NE_TIME_SCRIPT_EVENTS_HISTORY')
BEGIN
  CREATE TABLE [dbo].[0NE_TIME_SCRIPT_EVENTS_HISTORY](
    [ScriptID] [nvarchar](256) NOT NULL,
    [IssueDate] datetime NOT NULL,
    [AppliedOn] datetime NOT NULL,
    [Comments] [nvarchar](max) NOT NULL
  )
END

USE [VAD20]

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '0NE_TIME_SCRIPT_EVENTS')
BEGIN
  CREATE TABLE [dbo].[0NE_TIME_SCRIPT_EVENTS](
    [ScriptID] [nvarchar](256) NOT NULL,
    [IssueDate] datetime NOT NULL,
    [AppliedOn] datetime NOT NULL,
    [Comments] [nvarchar](max) NOT NULL,
   CONSTRAINT [PK_0NE_TIME_SCRIPT_EVENTS] PRIMARY KEY CLUSTERED 
  (
    [ScriptID] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '0NE_TIME_SCRIPT_EVENTS_HISTORY')
BEGIN
  CREATE TABLE [dbo].[0NE_TIME_SCRIPT_EVENTS_HISTORY](
    [ScriptID] [nvarchar](256) NOT NULL,
    [IssueDate] datetime NOT NULL,
    [AppliedOn] datetime NOT NULL,
    [Comments] [nvarchar](max) NOT NULL
  )
END

SET DATEFORMAT YMD

--------------------------------

-- Inicio OneTimeScript

USE [VAD20]

-- DECLARE @TmpVarScriptID NVARCHAR(256)
-- DECLARE @TmpVarIssueDate DATETIME
-- DECLARE @TmpVarApplyDate DATETIME
-- DECLARE @TmpVarScriptComments NVARCHAR(MAX)

SET @TmpVarScriptID = 'b4b6daff-ca5b-4fcb-81af-4a5337aa225a'
SET @TmpVarIssueDate = CAST('20170519 12:30:00.000' AS DATETIME)
SET @TmpVarApplyDate = GETDATE()
SET @TmpVarScriptComments = 'Script para Datos Iniciales de las Tablas de Servicios Novared.'

--DECLARE @TmpVarUpdateRow INT
--DECLARE @TmpVarQueryDate DATETIME 

IF EXISTS(SELECT * FROM [0NE_TIME_SCRIPT_EVENTS] WHERE ScriptID = @TmpVarScriptID)
BEGIN	
	
	SET @TmpVarQueryDate = (SELECT TOP (1) IssueDate FROM [0NE_TIME_SCRIPT_EVENTS_HISTORY] WHERE ScriptID = @TmpVarScriptID ORDER BY IssueDate DESC)
	
	IF (@TmpVarQueryDate IS NULL)
	BEGIN
		SET @TmpVarQueryDate = (SELECT TOP (1) IssueDate FROM [0NE_TIME_SCRIPT_EVENTS] WHERE ScriptID = @TmpVarScriptID ORDER BY IssueDate)		
	END
	
	IF (@TmpVarQueryDate >= @TmpVarIssueDate)
	BEGIN
		SET @TmpVarUpdateRow = 0
	END
	ELSE
	BEGIN
		SET @TmpVarUpdateRow = 1
	END
END
ELSE
BEGIN
	SET @TmpVarUpdateRow = 2
END

IF (@TmpVarUpdateRow = 1 OR @TmpVarUpdateRow = 2)
BEGIN

	-- En t�rminos generales el script debe ser capaz de ejecutarse deshaciendo cualquier cambio relacionado con ejecuciones previas.
	-- Por ejemplo si se trata de INSERTS de datos, hacer los correspondientes DELETES primero, si se trata de crear una tabla temporal,
	-- hacer el IF EXISTS -> DROP, antes del CREATE TABLE, o simplemente no borrarla sino hacer primero un TRUNCATE, si se trata de un
	-- cambio de esquema de tablas o varios comprobar con IF EXISTS dichos objetos para evitar errores, pero como la idea ser�a preservar
	-- la data de alg�n modo, esa es la idea de que el script solamente se ejecute una vez, lo cual es el prop�sito de este Script General, 
	
	-- Inicio del Script

	-- Tablas Novared para POS.

	USE [VAD20]

	SET ANSI_NULLS ON

	SET QUOTED_IDENTIFIER ON

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MA_NOVARED_ORDENES_PREPAGO]') AND type in (N'U'))
	BEGIN
		
		CREATE TABLE [dbo].[MA_NOVARED_ORDENES_PREPAGO](
			[IDTransStellar] [nvarchar](20) NOT NULL,
			[TipoServicio] [int] NOT NULL,
			[EstatusOrden] [int] NOT NULL CONSTRAINT [DF_MA_NOVARED_ORDENES_PREPAGO_EstatusOrden]  DEFAULT ((0)),
			[FechaTransaccion] [datetime] NOT NULL CONSTRAINT [DF_MA_NOVARED_ORDENES_PREPAGO_FechaTransaccion]  DEFAULT (getdate()),
			[MontoSolicitado] [float] NOT NULL,
			[IDUsuarioNovared] [nvarchar](50) NOT NULL,
			[ID_POS] [nvarchar](50) NOT NULL,
			[NumeroSerial] [nvarchar](100) NOT NULL,
			[CodigoRegistro] [nvarchar](100) NOT NULL,
			[PinUsuario] [nvarchar](100) NOT NULL,
			[IDCliente] [nvarchar](50) NOT NULL,
			[Proveedor_Serial] [nvarchar](100) NOT NULL,
			[Proveedor_Pin] [nvarchar](100) NOT NULL,
			[Proveedor_Lote] [nvarchar](100) NOT NULL,
			[MontoConfirmado] [float] NOT NULL CONSTRAINT [DF_MA_NOVARED_ORDENES_PREPAGO_MontoConfirmado]  DEFAULT ((0)),
			[DiasProg] [int] NOT NULL CONSTRAINT [DF_MA_NOVARED_ORDENES_PREPAGO_DiasProg]  DEFAULT ((0)),
			[IDCajero] [nvarchar](20) NOT NULL,
			[c_Caja] [nvarchar](20) NOT NULL,
			[Turno] [float] NOT NULL,
			[TurnoCierre] [float] NOT NULL CONSTRAINT [DF_MA_NOVARED_ORDENES_PREPAGO_TurnoCierre]  DEFAULT ((0)),
			[ID] [int] IDENTITY(1,1) NOT NULL,
		 CONSTRAINT [PK_MA_NOVARED_ORDENES_PREPAGO] PRIMARY KEY CLUSTERED 
		(
			[NumeroSerial] ASC,
			[TipoServicio] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

	END

	SET ANSI_NULLS ON

	SET QUOTED_IDENTIFIER ON

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MA_NOVARED_ORDENES_PREPAGO_ESTATUS]') AND type in (N'U'))
	BEGIN
		
		CREATE TABLE [dbo].[MA_NOVARED_ORDENES_PREPAGO_ESTATUS](
			[EstatusOrdenID] [int] NOT NULL,
			[Descripcion] [nvarchar](100) NOT NULL,
			CONSTRAINT [PK_MA_NOVARED_ORDENES_PREPAGO_ESTATUS] PRIMARY KEY CLUSTERED 
		(
			[EstatusOrdenID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		INSERT [dbo].[MA_NOVARED_ORDENES_PREPAGO_ESTATUS] ([EstatusOrdenID], [Descripcion]) VALUES (-1, N'Orden Anulada / Reversada')
		INSERT [dbo].[MA_NOVARED_ORDENES_PREPAGO_ESTATUS] ([EstatusOrdenID], [Descripcion]) VALUES (0, N'Orden Agregada (Pendiente)')
		INSERT [dbo].[MA_NOVARED_ORDENES_PREPAGO_ESTATUS] ([EstatusOrdenID], [Descripcion]) VALUES (1, N'Orden Confirmada (Completada)')
		INSERT [dbo].[MA_NOVARED_ORDENES_PREPAGO_ESTATUS] ([EstatusOrdenID], [Descripcion]) VALUES (2, N'Desconocido (Estatus conocido solo por el Proveedor de Servicio)')

	END

	/****** Object:  Table [dbo].[MA_BLACKSTONE_PREPAID_STATUS]    Script Date: 5/19/2016 11:31:56 AM ******/
	SET ANSI_NULLS ON

	SET QUOTED_IDENTIFIER ON

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MA_NOVARED_ORDENES_PREPAGO_TIPO_SERVICIO]') AND type in (N'U'))
	BEGIN
		
		CREATE TABLE [dbo].[MA_NOVARED_ORDENES_PREPAGO_TIPO_SERVICIO](
			[TipoServicioID] [int] NOT NULL,
			[Descripcion] [nvarchar](250) NOT NULL,
			CONSTRAINT [PK_MA_NOVARED_ORDENES_PREPAGO_TIPO_SERVICIO] PRIMARY KEY CLUSTERED 
		(
			[TipoServicioID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		INSERT [dbo].[MA_NOVARED_ORDENES_PREPAGO_TIPO_SERVICIO] ([TipoServicioID], [Descripcion]) VALUES (1, N'Recargas Generales (Telefon�a. Televisi�n Satelital, etc)')

	END

	-- Fin del Script
	
END

IF (@TmpVarUpdateRow = 2)
BEGIN
	INSERT INTO [0NE_TIME_SCRIPT_EVENTS] (ScriptID, IssueDate, AppliedOn, Comments)
	VALUES (@TmpVarScriptID, @TmpVarIssueDate, @TmpVarApplyDate, @TmpVarScriptComments)
	
	INSERT INTO [0NE_TIME_SCRIPT_EVENTS_HISTORY] (ScriptID, IssueDate, AppliedOn, Comments)
	VALUES (@TmpVarScriptID, @TmpVarIssueDate, @TmpVarApplyDate, @TmpVarScriptComments)
END

IF (@TmpVarUpdateRow = 1)
BEGIN
	UPDATE [0NE_TIME_SCRIPT_EVENTS] SET IssueDate = @TmpVarIssueDate, AppliedOn = @TmpVarApplyDate
	WHERE ScriptID = @TmpVarScriptID 
	
	INSERT INTO [0NE_TIME_SCRIPT_EVENTS_HISTORY] (ScriptID, IssueDate, AppliedOn, Comments)
	VALUES (@TmpVarScriptID, @TmpVarIssueDate, @TmpVarApplyDate, @TmpVarScriptComments)
END

IF (@TmpVarUpdateRow = 0)
BEGIN
	PRINT 'El Script [' + @TmpVarScriptID + '] ya se ha ejecutado anteriormente, por lo cual ya cumpli� su prop�sito y no se ejecutar� nuevamente.'
END

-- Fin OneTimeScript
